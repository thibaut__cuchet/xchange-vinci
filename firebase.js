import { initializeApp } from "firebase/app";
import { getStorage, ref, uploadBytes, getDownloadURL } from "firebase/storage";
import uuid from "uuid";

const firebaseConfig = {
  apiKey:
    "AAAAbzyX4pM:APA91bE0jLqqY9H2UvDlyOsBdv8fYSoNPxGT598Syrvarsk1Xl1BnNDivHhDqMPcIFOh3ecE9pJ-VkLD1yjD7h_rW0_MbdcalYiVwxJKTDozeK9j8ZoO3JDHH-fc-90MM56TUkEsgqB8",
  authDomain: "xchange-334414.firebaseapp.com",
  projectId: "xchange-334414",
  storageBucket: "gs://xchange-334414.appspot.com",
  messagingSenderId: "477757956755",
  appId: "1:477757956755:android:7fa5d810d511b76aca2366",
};

const app = initializeApp(firebaseConfig);
const storage = getStorage(app);

export const UploadImage = async (uri) => {
  const response = await fetch(uri);
  const blob = await response.blob();
  const reference = ref(storage, uuid.v4());
  return await uploadBytes(reference, blob).then(async (snapshot) => {
    return await getDownloadURL(snapshot.ref);
  });
};
