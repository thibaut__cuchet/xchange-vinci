# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 1.1.0 (2021-12-08)


### Features

* add bottom router ([7203361](https://bitbucket.org/thibaut__cuchet/xchange-vinci/commit/72033610f69d069a9633e662a688bf350f28f746))
* add label in bottom bar ([7e01b26](https://bitbucket.org/thibaut__cuchet/xchange-vinci/commit/7e01b26a25b9ea8417729a40ad7e4cfe710df987))
* add package to build ([87fa036](https://bitbucket.org/thibaut__cuchet/xchange-vinci/commit/87fa0361c9245314025c9ccb5aa10d4cec5aa6e2))
* add tests to pipelines ([389adfe](https://bitbucket.org/thibaut__cuchet/xchange-vinci/commit/389adfe034847a42bed010863bf18f30d3bc6390))
* auto publish to Play Store ([c452a5a](https://bitbucket.org/thibaut__cuchet/xchange-vinci/commit/c452a5a336e2d6c4043ed88008b9b0438e0f2716))
* init repository ([88fc1ef](https://bitbucket.org/thibaut__cuchet/xchange-vinci/commit/88fc1ef2ac4e97f3b438deeb1a90ac0ddda28316))
* update bottom router ([c602ae6](https://bitbucket.org/thibaut__cuchet/xchange-vinci/commit/c602ae6d59a273f6e1f814036304090876f8bbea))


### Bug Fixes

* pipeline expo-cli missing ([5317604](https://bitbucket.org/thibaut__cuchet/xchange-vinci/commit/5317604701090dba5ff1717328cfce4071edc90e))
