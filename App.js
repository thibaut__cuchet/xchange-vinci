import React from "react";
import { StackNavigation } from "./src/components/StackNavigation";
import { NavigationContainer } from "@react-navigation/native";
import AppLoading from "expo-app-loading";
import "rn-overlay";
import {
  useFonts,
  GentiumBasic_400Regular,
  GentiumBasic_700Bold,
} from "@expo-google-fonts/gentium-basic";
import { QueryClientProvider, QueryClient } from "react-query";
import { queryBuilders } from "./src/utils/query";

export default function App() {
  let [fontsLoaded] = useFonts({
    GentiumBasic_400Regular,
    GentiumBasic_700Bold,
  });

  if (!fontsLoaded) {
    return <AppLoading />;
  }
  const queryClient = new QueryClient();

  return (
    <NavigationContainer>
      <QueryClientProvider client={queryClient}>
        <StackNavigation />
      </QueryClientProvider>
    </NavigationContainer>
  );
}
