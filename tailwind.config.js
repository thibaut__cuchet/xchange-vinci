module.exports = {
  purge: [],
  darkMode: "media",
  theme: {
    extend: {
      colors: {
        primary: "#058AAE",
        secondary: "#119db8",
        accent: "#b7e0f0",
        intro: "#032634",
        "vinci-rose": "#E7276D",
        "vinci-jaune": "#F8AA00",
        "vinci-vert": "#009670",
      },
      flex: {
        2: "2 2 0%",
        3: "3 3 0%",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
