import React, { useState } from "react";
import { View, Text } from "react-native";
import { AdvertisementsList } from "../components/AdvertisementsList";
import { favoriteProductsQuery } from "../queries/products";
import tw from "../utils/tailwind";

export const FavoritesList = ({ stackNavigation }) => {
  const query = favoriteProductsQuery({});

  return (
    <View>
      <View style={tw`m-4 pt-10`}>
        <AdvertisementsList
          query={query}
          advertisements={
            query.isLoading
              ? []
              : query.data.data.data["users_favorites_advertisements"].map(
                  (item) => item.advertisement
                )
          }
          action={(id) =>
            stackNavigation.navigate("ProductView", {
              productId: id,
            })
          }
        />
      </View>
    </View>
  );
};
