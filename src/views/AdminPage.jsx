import React, { useState, useEffect } from "react";
import { AppText } from "../components/AppText";
import {
  FlatList,
  View,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView,
  Text,
} from "react-native";
import { TextButton } from "../components/AppButton";
import tw from "../utils/tailwind";
import { BanIcon } from "../components/Icons";
import { allCategoriesQuery } from "../queries/categories";
import { allUsersQuery } from "../queries/users";
import {
  addCategoryQuery,
  lockUserQuery,
  rejectProductQuery,
  removeCategoryQuery,
  signaledProductsQuery,
} from "../queries/products";
import { useQueryClient } from "react-query";

export const AdminPage = ({ navigation }) => {
  const [swip, setSwip] = useState("R");

  return (
    <View style={tw`flex justify-center items-center h-full w-full bg-primary`}>
      <View
        style={tw`flex-col justify-center items-center h-5/6 w-5/6 bg-accent rounded-2xl shadow-2xl`}
      >
        <View style={tw`flex justify-center items-center h-1/12 w-full`}>
          <AppText style={{ ...tw`text-center text-primary`, fontSize: 30 }}>
            Administration
          </AppText>
        </View>
        <View style={tw`flex-col justify-center items-center h-5/6 w-9/10`}>
          <View style={tw`flex-row h-1/12 w-full bg-accent`}>
            <ModerationTabButton
              tab="R"
              color="bg-primary"
              swip={swip}
              setSwip={setSwip}
            />
            <ModerationTabButton
              tab="U"
              color="bg-vinci-vert"
              swip={swip}
              setSwip={setSwip}
            />
            <ModerationTabButton
              tab="C"
              color="bg-vinci-jaune"
              swip={swip}
              setSwip={setSwip}
            />
            <ModerationTabButton
              tab="N"
              color="bg-vinci-rose"
              swip={swip}
              setSwip={setSwip}
            />
          </View>
          <View style={{ ...tw`flex h-11/12 w-full bg-yellow-50` }}>
            {swip === "U" ? (
              <UsersList />
            ) : swip === "R" ? (
              <ReportList />
            ) : swip === "C" ? (
              <CategoryList />
            ) : (
              <NewCategory />
            )}
          </View>
        </View>
        <View style={tw`flex justify-center items-center h-1/12 w-full`}>
          <TextButton
            containerStyle={tw`flex justify-center items-center h-full w-full`}
            buttonStyle={tw`bg-yellow-50 justify-center border-t-2 border-primary items-center rounded-b-lg h-full w-full`}
            text="Revenir au Profil"
            textStyle={{
              color: "#058AAE",
              fontSize: 30,
            }}
            touchColor={"#058AAE"}
            action={() => {
              navigation.goBack();
            }}
          />
        </View>
      </View>
    </View>
  );
};

const ModerationTabButton = ({ tab, color, swip, setSwip }) => {
  return (
    <TextButton
      containerStyle={tw`flex justify-start items-center h-full w-1/4`}
      buttonStyle={tw`${
        swip !== tab ? `${color} border-4 border-accent` : "bg-yellow-50"
      }  justify-center items-center h-full w-full`}
      touchColor={"#fefce8"}
      action={() => {
        setSwip(tab);
      }}
    />
  );
};

const List = ({ data, refreshing, onRefresh, keyExtractor, renderItem }) => {
  return (
    <FlatList
      data={data}
      style={tw`h-full w-full`}
      refreshing={refreshing}
      onRefresh={onRefresh}
      showsVerticalScrollIndicator={false}
      keyExtractor={keyExtractor}
      renderItem={renderItem}
    />
  );
};

const ListItem = ({ title, subtitle, onPress }) => {
  return (
    <View
      style={tw`flex-col justify-center items-start w-full border-accent border-b p-4`}
    >
      <View style={tw`flex w-4/5`}>
        <AppText style={{ ...tw`text-primary`, fontSize: 16 }}>{title}</AppText>
        {subtitle && (
          <AppText style={{ ...tw`text-primary`, fontSize: 16 }}>
            {subtitle}
          </AppText>
        )}
      </View>
      <View style={tw`absolute justify-center items-center right-4`}>
        <TouchableOpacity
          onPress={onPress}
          style={tw`rounded-full h-10 w-10 justify-center items-center bg-accent bg-opacity-40`}
        >
          <BanIcon color="red" style={tw`h-10 w-10`} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const CategoryList = () => {
  const queryCategories = allCategoriesQuery({});
  const mutation = removeCategoryQuery({
    options: {
      onSuccess: () => queryCategories.refetch(),
    },
  });

  if (queryCategories.isLoading) return <View />;
  const categories = queryCategories.data.data.data.categories;

  return (
    <View style={{ ...tw`flex-col justify-center items-center h-full w-full` }}>
      <View style={{ ...tw`flex justify-center items-center h-1/10 w-full` }}>
        <AppText style={{ ...tw`text-center text-primary`, fontSize: 24 }}>
          Catégories
        </AppText>
      </View>
      <View style={{ ...tw`flex justify-center items-center h-9/10 w-full` }}>
        <List
          data={categories}
          refreshing={queryCategories.isLoading}
          onRefresh={() => queryCategories.refetch()}
          keyExtractor={(item) => item.id}
          renderItem={({ item }) => (
            <ListItem
              title={item.name}
              onPress={() => mutation.mutate(item.id)}
            />
          )}
        />
      </View>
    </View>
  );
};

const SelectCategories = ({
  categories,
  setParentCategory,
  parentCategory,
}) => (
  <FlatList
    data={categories}
    style={tw`w-full`}
    showsVerticalScrollIndicator={false}
    keyExtractor={(item) => item.id.toString()}
    renderItem={({ item }) => (
      <View
        style={{
          ...tw`flex-col justify-center items-start w-full border-accent border-b p-3`,
        }}
      >
        <View
          style={{
            ...tw`flex w-full`,
          }}
        >
          <TextButton
            action={() => setParentCategory(item.id)}
            textStyle={
              item.id === parentCategory
                ? {
                    ...tw`text-vinci-vert text-center`,
                    fontSize: 20,
                  }
                : {
                    ...tw`text-primary text-center`,
                    fontSize: 18,
                  }
            }
            containerStyle={{
              ...tw`flex w-full`,
            }}
            buttonStyle={{
              ...tw`bg-yellow-50`,
            }}
            touchColor="#009670"
            text={item.name}
          />
        </View>
      </View>
    )}
  />
);

const NewCategory = () => {
  const [parentCategory, setParentCategory] = useState(null);
  const [newCategory, setNewCategory] = useState("");
  const [selectParent, setSelectParent] = useState(false);

  const queryClient = useQueryClient();
  const queryCategories = allCategoriesQuery({});
  const mutation = addCategoryQuery({
    options: {
      onSuccess: () => {
        setNewCategory("");
        setParentCategory(null);
        setSelectParent(false);
        alert("Catégorie ajoutée");
        queryClient.invalidateQueries("allCategories");
      },
    },
  });

  if (queryCategories.isLoading) return <View />;
  const categories = queryCategories.data.data.data.categories.filter(
    (cat) => !cat.parent
  );

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
    >
      <View style={tw`h-full flex flex-col`}>
        <AppText style={tw`text-center text-primary text-2xl mt-4`}>
          Création Catégorie
        </AppText>
        <View style={tw`p-4`}>
          <TextInput
            onChangeText={(cat) => setNewCategory(cat)}
            placeholder="Nouvelle catégorie"
            style={{
              ...tw`w-full bg-accent bg-opacity-50 p-2 rounded`,
              fontFamily: "GentiumBasic_400Regular",
              fontSize: 18,
              color: "#058AAE",
            }}
            value={newCategory}
          />
        </View>
        <View style={tw`flex items-center flex-1 w-full px-5`}>
          <TextButton
            action={() => {
              setParentCategory(null);
              setSelectParent(!selectParent);
            }}
            textStyle={
              selectParent
                ? {
                    ...tw`text-vinci-vert text-center`,
                    fontSize: 16,
                  }
                : {
                    ...tw`text-primary text-center`,
                    fontSize: 16,
                  }
            }
            containerStyle={tw`w-full flex justify-center items-center`}
            buttonStyle={tw` bg-accent rounded-2xl py-2 px-3`}
            touchColor="#009670"
            text="Sélectionner catégorie parent"
          />
          {selectParent && (
            <SelectCategories
              categories={categories}
              setParentCategory={setParentCategory}
              parentCategory={parentCategory}
            />
          )}
        </View>
        <View>
          <TextButton
            textStyle={{
              ...tw`text-primary text-center`,
              fontSize: 20,
              letterSpacing: 0.5,
            }}
            containerStyle={tw`flex w-full items-center justify-center mb-2`}
            buttonStyle={tw`bg-accent w-2/3 px-4 py-1 rounded`}
            touchColor="#006f91"
            text="Ajouter"
            action={() => {
              mutation.mutate({
                name: newCategory,
                parent: parentCategory,
              });
            }}
          />
        </View>
      </View>
    </KeyboardAvoidingView>
  );
};

const ReportList = () => {
  const queryReports = signaledProductsQuery({});
  const mutation = rejectProductQuery({
    options: {
      onSuccess: () => queryReports.refetch(),
    },
  });

  if (queryReports.isLoading) return <View />;
  const reports = queryReports.data.data.data.signaled_advertisements.filter(
    (adv) => adv.advertisement.state === "published"
  );

  return (
    <View style={{ ...tw`flex-col justify-center items-center h-full w-full` }}>
      <View style={{ ...tw`flex justify-center items-center h-1/10 w-full` }}>
        <AppText style={{ ...tw`text-center text-primary`, fontSize: 24 }}>
          Signalements
        </AppText>
      </View>
      <View style={{ ...tw`flex justify-center items-center h-9/10 w-full` }}>
        <List
          data={reports}
          refreshing={queryReports.isLoading}
          onRefresh={() => queryReports.refetch()}
          keyExtractor={(item) => item.advertisement.id}
          renderItem={({ item }) => (
            <ListItem
              title={item.advertisement.title}
              subtitle={item.advertisement.user.email}
              onPress={() => mutation.mutate(item.advertisement.id)}
            />
          )}
        />
      </View>
    </View>
  );
};

const UsersList = () => {
  const queryUsers = allUsersQuery({});
  const mutation = lockUserQuery({
    options: {
      onSuccess: queryUsers.refetch(),
    },
  });

  if (queryUsers.isLoading) return <View />;
  const users = queryUsers.data.data.data.users.filter(
    (user) => !user.is_locked
  );

  return (
    <View style={{ ...tw`flex-col justify-center items-center h-full w-full` }}>
      <View style={{ ...tw`flex justify-center items-center h-1/10 w-full` }}>
        <AppText style={{ ...tw`text-center text-primary`, fontSize: 24 }}>
          Utilisateurs
        </AppText>
      </View>
      <View style={{ ...tw`flex justify-center items-center h-9/10 w-full` }}>
        <List
          data={users}
          refreshing={queryUsers.isLoading}
          onRefresh={() => queryUsers.refetch()}
          keyExtractor={(item) => item.id}
          renderItem={({ item }) => (
            <ListItem
              title={`${item.last_name} ${item.first_name}`}
              subtitle={item.email}
              onPress={() => mutation.mutate(item.id)}
            />
          )}
        />
      </View>
    </View>
  );
};
