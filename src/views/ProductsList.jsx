import React, { useContext, useState, useEffect } from "react";

import tw from "../utils/tailwind";
import { View } from "react-native";
import { AdvertisementsList } from "../components/AdvertisementsList";
import { allProductsQuery } from "../queries/products";
import { ProductsListHeader } from "../components/ProductsListHeader";
import { Filter } from "../components/Filter";
import Context from "../context/FilterContext";

export const ProductsList = ({ stackNavigation }) => {
  const [showFilters, setShowFilters] = useState(false);
  const [panelActive, setPanelActive] = useState(false);
  const [search, setSearch] = useState("");

  const { filters } = useContext(Context);

  const query = allProductsQuery({
    filters,
    search,
  });
  const handleFilterOnPress = () => {
    setPanelActive(true);
  };

  return (
    <View style={tw`h-full`}>
      <Filter panelActive={panelActive} setPanelActive={setPanelActive} />
      <View>
        <ProductsListHeader
          showFilters={showFilters}
          setShowFilters={setShowFilters}
          handleFilterOnPress={handleFilterOnPress}
          setSearch={setSearch}
        />
      </View>
      <View style={tw`m-5 mt-0`}>
        <View style={tw`flex h-full w-full pb-32`}>
          <AdvertisementsList
            action={(id) =>
              stackNavigation.navigate("ProductView", {
                productId: id,
              })
            }
            query={query}
            advertisements={
              query.isLoading ? [] : query.data.data.data.advertisements
            }
          />
        </View>
      </View>
    </View>
  );
};
