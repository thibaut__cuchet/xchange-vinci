import React, { useState, useEffect } from "react";
import { View, Text } from "react-native";
import { AdvertisementsList } from "../components/AdvertisementsList";
import { ReturnButton } from "../components/ReturnButton";
import { myOrdersQuery } from "../queries/products";
import tw from "../utils/tailwind";

export const MyOrdersList = ({ navigation }) => {
  const [advertisements, setAdvertisements] = useState([]);
  const query = myOrdersQuery({});

  useEffect(() => {
    if (query.isFetched) {
      setAdvertisements(
        query.data.data.data.sales.map((sale) => sale.advertisement)
      );
    }
  }, [query.isFetched]);

  return (
    <View>
      <View style={tw`m-4`}>
        <ReturnButton
          onPress={() => {
            navigation.goBack();
          }}
        />
      </View>
      <View style={tw`pb-24`}>
        <AdvertisementsList
          query={query}
          advertisements={advertisements}
          action={(id) =>
            navigation.navigate("ProductView", {
              productId: id,
            })
          }
          ListHeaderComponent={() => <View style={tw`h-14`}></View>}
        />
      </View>
    </View>
  );
};
