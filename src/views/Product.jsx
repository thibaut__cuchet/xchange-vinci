import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { useState } from "react";
import { SliderBox } from "react-native-image-slider-box";
import tw from "../utils/tailwind";
import { DetailsProduct } from "../components/DetailsProduct";
import { ProductPhotosSliderBox } from "../components/ProductPhotosSliderBox";
import { productDetailsQuery } from "../queries/products";
import { ReturnButton } from "../components/ReturnButton";

export const Product = ({ route, navigation }) => {
  const [selectedImage, setSelectedImage] = useState();
  const { productId } = route.params;
  const query = productDetailsQuery({ productId });
  if (query.isLoading) return <View />;
  const detailsProduct = query.data.data.data.advertisements[0];
  const images = query.data.data.data.advertisements[0].pictures;
  return (
    <View style={tw` flex h-full w-full bg-yellow-50`}>
      {selectedImage ? (
        <ProductPhotosSliderBox
          setSelectedImage={setSelectedImage}
          images={images}
        />
      ) : (
        <View style={tw`m-4`}>
          <ReturnButton
            onPress={() => {
              navigation.goBack();
            }}
          />
        </View>
      )}
      <View style={tw`h-1/3 w-full`}>
        <SliderBox
          style={tw`h-full w-full`}
          images={images.map((img) => img.url)}
          dotColor="#006f91"
          inactiveDotColor="#ccc"
          autoplay={true}
          onCurrentImagePressed={(index) => setSelectedImage(images[index])}
          circleLoop={true}
          dotStyle={{
            width: 15,
            height: 15,
            borderRadius: 15,
            marginHorizontal: 10,
            padding: 0,
            margin: 0,
          }}
        />
      </View>
      <DetailsProduct details={detailsProduct} />
    </View>
  );
};
