import React, { useRef, useState } from "react";
import { View, TextInput, KeyboardAvoidingView } from "react-native";
import { DismissKeyboard } from "../components/DismissKeyboard";
import { ImplementationsPicker } from "../components/ImplementationsPicker";
import { TextButton } from "../components/AppButton";
import tw from "../utils/tailwind";
import { AppText } from "../components/AppText";
import { useMutation } from "react-query";
import { registerRequest } from "../services/authentification";
import AsyncStorage from "@react-native-async-storage/async-storage";

export const Register = ({ navigation }) => {
  const [lastName, setLastName] = useState("");
  const [firstName, setFirtsName] = useState("");
  const refFirstName = useRef();
  const [email, setEmail] = useState("");
  const refEmail = useRef();
  const [implementation, setImplementation] = useState("");
  const [password, setPassword] = useState("");
  const [verificationPassword, setVerificationPassword] = useState("");
  const refValidationPassword = useRef();
  const [errorMessage, setErrorMessage] = useState("");

  const checkForm = () => {
    if (fieldsEmpty()) {
      setErrorMessage("Tous les champs doivent être remplis !");
      return;
    }
    if (password != verificationPassword) {
      return;
    }
    if (!isVinciEmail()) {
      return;
    }
    setErrorMessage("Form envoyé");
    mutation.mutate({
      first_name: firstName,
      last_name: lastName,
      email,
      password,
      implementation_id: implementation,
    });
  };

  const mutation = useMutation(
    (registerInfo) => registerRequest(registerInfo),
    {
      onSuccess: async (response) => {
        const data = response.data;
        if (!data.data) return;
        if (data.data.register.token) {
          AsyncStorage.setItem("TOKEN", data.data.register.token);
          navigation.replace("Home");
        }
      },
      onError: (error) => console.log(error),
    }
  );

  const isVinciEmail = () => {
    const studentRegex = /^.+@student.vinci.be$/;
    const staffRegex = /^.+@vinci.be$/;
    return studentRegex.test(email) || staffRegex.test(email);
  };

  const fieldsEmpty = () => {
    return (
      lastName === "" ||
      firstName === "" ||
      email === "" ||
      implementation === "" ||
      password === "" ||
      verificationPassword === ""
    );
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
    >
      <DismissKeyboard>
        <View
          style={tw`bg-primary h-full w-full flex flex-col items-center justify-center`}
        >
          <AppText style={tw`text-yellow-50 text-5xl mb-3`}>
            Inscription
          </AppText>
          <View
            style={tw`flex items-center justify-center bg-accent w-3/4 rounded-lg shadow-2xl`}
          >
            <View style={tw`flex items-center justify-center w-full`}>
              <TextInput
                style={{
                  ...tw`bg-yellow-50 mb-3 mt-5 w-3/4 p-3 rounded-lg shadow-lg`,
                  fontFamily: "GentiumBasic_400Regular",
                  fontSize: 18,
                  color: "#009670",
                }}
                placeholderTextColor="#006f91"
                placeholder="Nom"
                returnKeyType="next"
                autoComplete="lastname"
                onChangeText={setLastName}
                onSubmitEditing={() => refFirstName.current.focus()}
              />
              <TextInput
                style={{
                  ...tw`bg-yellow-50 my-3 w-3/4 p-3 rounded-lg shadow-lg`,
                  fontFamily: "GentiumBasic_400Regular",
                  fontSize: 18,
                  color: "#009670",
                }}
                placeholderTextColor="#006f91"
                placeholder="Prénom"
                returnKeyType="next"
                autoComplete="firstname"
                ref={refFirstName}
                onChangeText={setFirtsName}
                onSubmitEditing={() => refEmail.current.focus()}
              />
              <TextInput
                style={{
                  ...tw`bg-yellow-50 my-3 w-3/4 p-3 rounded-lg shadow-lg`,
                  fontFamily: "GentiumBasic_400Regular",
                  fontSize: 18,
                  color: "#009670",
                }}
                placeholderTextColor="#006f91"
                placeholder="Adresse mail"
                returnKeyType="next"
                keyboardType="email-address"
                autoCapitalize="none"
                autoComplete="email"
                ref={refEmail}
                onChangeText={setEmail}
              />
              <TextInput
                style={{
                  ...tw`bg-yellow-50 my-3 w-3/4 p-3 rounded-lg shadow-lg`,
                  fontFamily: "GentiumBasic_400Regular",
                  fontSize: 18,
                  color: "#009670",
                }}
                secureTextEntry={true}
                placeholderTextColor="#006f91"
                placeholder="Mot de passe"
                onChangeText={setPassword}
                onSubmitEditing={() => refValidationPassword.current.focus()}
              />
              <TextInput
                style={{
                  ...tw`bg-yellow-50 my-3 w-3/4 p-3 rounded-lg shadow-lg`,
                  fontFamily: "GentiumBasic_400Regular",
                  fontSize: 18,
                  color: "#009670",
                }}
                secureTextEntry={true}
                placeholderTextColor="#006f91"
                placeholder="Vérification mot de passe"
                ref={refValidationPassword}
                onChangeText={setVerificationPassword}
              />
              <View style={tw`w-3/4 bg-yellow-50 rounded-lg shadow-lg my-3`}>
                <ImplementationsPicker
                  implementation={implementation}
                  setImplementation={setImplementation}
                />
              </View>
              <View style={tw`mb-1`}>
                <AppText style={tw`text-red-500`}>{errorMessage}</AppText>
              </View>
              <View style={tw`items-center mb-3`}>
                <TextButton
                  textStyle={{
                    fontSize: 14,
                    color: "#006f91",
                    textDecorationLine: "underline",
                  }}
                  text="Déjà un compte ? Connectez-vous ici !"
                  action={() => navigation.navigate("Login")}
                />
              </View>
            </View>
            <View
              style={tw`flex bg-white w-full h-15 rounded-b-lg justify-center`}
            >
              <TextButton
                containerStyle={{
                  ...tw`flex justify-center items-center h-full w-full`,
                }}
                buttonStyle={{
                  ...tw`bg-yellow-50 justify-center border-t-2 border-primary items-center rounded-b-lg h-full w-full`,
                }}
                text="S'inscire"
                textStyle={{ fontSize: 22, color: "#058AAE" }}
                touchColor="#058AAE"
                action={checkForm}
              />
            </View>
          </View>
        </View>
      </DismissKeyboard>
    </KeyboardAvoidingView>
  );
};
