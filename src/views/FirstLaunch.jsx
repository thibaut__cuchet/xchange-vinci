import React from "react";
import { View, Text, Image } from "react-native";
import AppIntroSlider from "react-native-app-intro-slider";
import { Check, LeftChevron, RightChevron } from "../components/Icons";
import tw from "../utils/tailwind";
import AsyncStorage from "@react-native-async-storage/async-storage";

const slides = [
  {
    key: 1,
    text: `Nous avons tous des trésors enfouis au fond de notre grenier... \nNous pourrions leur donner une seconde vie...`,
    image: require("../../assets/logo_vinci.png"),
    imageStyle: "w-60 h-54",
    backgroundColor: "primary",
  },
  {
    key: 2,
    text: `Cette plateforme Vinci vous permet d'économiser de l'argent`,
    image: require("../../assets/money-saving.png"),
    imageStyle: "w-85 h-85",
    backgroundColor: "vinci-vert",
  },
  {
    key: 3,
    text: `En achetant, vendant ou donnant d'anciens objets que vous n'utilisez plus`,
    image: require("../../assets/buy.png"),
    imageStyle: "w-85 h-85",
    backgroundColor: "vinci-rose",
  },
  {
    key: 4,
    text: `En plus, vous agissez face à la surconsommation qui a un énorme impact écologique`,
    image: require("../../assets/recycling.png"),
    imageStyle: "w-85 h-85",
    backgroundColor: "vinci-jaune",
  },
];

export const FirstLaunch = ({ navigation }) => {
  const _renderItem = ({ item }) => {
    return (
      <View
        style={tw`bg-${item.backgroundColor} h-full flex items-center justify-center `}
      >
        <View style={tw`h-3/4 flex items-center justify-around `}>
          <Image
            style={tw`content-center ${item.imageStyle}`}
            source={item.image}
          />
          <Text style={tw`text-white text-center text-lg px-5`}>
            {item.text}
          </Text>
        </View>
      </View>
    );
  };

  return (
    <AppIntroSlider
      renderItem={_renderItem}
      data={slides}
      showPrevButton="true"
      renderNextButton={() => <RightChevron color={"white"} />}
      renderPrevButton={() => <LeftChevron color={"white"} />}
      renderDoneButton={() => <Check color={"white"} />}
      dotStyle={tw`w-5 h-5 bg-gray-500 rounded-full`}
      activeDotStyle={tw` w-5 h-5 bg-white rounded-full`}
      onDone={() => {
        AsyncStorage.setItem("LAUNCHED", "true");
        navigation.goBack();
      }}
    />
  );
};
