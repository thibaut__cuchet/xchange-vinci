import React, { useEffect, useState } from "react";
import { View, TextInput, Alert } from "react-native";
import { TextButton } from "../components/AppButton";
import { AppInput } from "../components/AppInput";
import tw from "../utils/tailwind";
import { currentUserQuery } from "../queries/users";
import { AppText } from "../components/AppText";
import { ImplementationsPicker } from "../components/ImplementationsPicker";
import { SwipeablePanel } from "rn-swipeable-panel";

export const EditProfile = ({ navigation }) => {
  const [iosImplementation, setIosImplementation] = useState(false);
  const [modification, setModification] = useState(false);
  const [lastName, setLastName] = useState("");
  const [firstName, setFirstName] = useState("");
  const [passWord, setPassWord] = useState("");
  const [confirmation, setConfirmation] = useState("");
  const [user, setUser] = useState(null);
  const [implementation, setImplementation] = useState(null);

  const query = currentUserQuery({});

  useEffect(() => {
    if (query.isFetched) {
      setUser(query.data.data.data.users[0]);
      setImplementation(query.data.data.data.users[0].implementation);
    }
  }, [query.isFetched]);

  const regText = /^[a-zéè '-]+$/i;
  const regPass = /^.{5,30}$/;

  const verification = () => {
    let verif = true;
    if (lastName !== "" && !regText.test(lastName)) {
      Alert.alert(
        "Modification du nom incorrect",
        "Les caractères spéciaux et les chiffres sont proscrits"
      );
      verif = false;
    }
    if (firstName !== "" && !regText.test(firstName)) {
      Alert.alert(
        "Modification du prénom incorrect",
        "Les caractères spéciaux et les chiffres sont proscrits"
      );
      verif = false;
    }
    if (passWord !== "") {
      if (confirmation === "" || passWord !== confirmation) {
        Alert.alert(
          "Modification du mot de passe incorrect",
          "Le mot de passe et sa confirmation sont inégals"
        );
        verif = false;
      }
      if (!regPass.test(passWord)) {
        Alert.alert(
          "Modification du mot de passe incorrect",
          "Votre mot de passe doit contenir entre 5 et 30 caractères"
        );
        verif = false;
      }
    }
    return verif;
  };

  const updateProfile = () => {
    if (lastName === "") setLastName(user.last_name);

    if (firstName === "") setFirstName(user.first_name);

    if (implementation === null) setImplementation(user.implementation);

    if (passWord === "") {
      //requete d'update avec update lastname firstname implementation sans password
    } else {
      //requete d'update avec update lastname firstname implementation avec update password
    }
  };

  const Implementations = () => {
    if (Platform.OS === "ios") {
      if (iosImplementation === false) {
        return (
          <View
            style={{ ...tw`justify-center items-center h-1/5 w-full py-3` }}
          >
            <TextButton
              action={() => setIosImplementation(true)}
              textStyle={{
                ...tw`text-primary text-center`,
                fontSize: 20,
              }}
              containerStyle={{
                ...tw`justify-center bg-yellow-50 p h-2/3 w-4/5`,
              }}
              buttonStyle={{
                ...tw`bg-yellow-50 `,
              }}
              touchColor="#006f91"
              text="Implémentations"
            />
          </View>
        );
      }
      return (
        <View
          style={tw`absolute justify-center items-center bg-yellow-50 border-4 border-accent w-full h-full bottom-0 z-10`}
        >
          <View style={{ ...tw`justify-center bg-yellow-50 p-2 h-2/3 w-4/5` }}>
            <ImplementationsPicker
              implementation={implementation}
              setImplementation={(item) => {
                setImplementation(item);
                setModification(true);
                setIosImplementation(false);
              }}
              iconColor={"#058AAE"}
              pickerStyle={
                modification
                  ? {
                      ...tw`h-full w-full`,
                      color: "#009670",
                    }
                  : {
                      ...tw`h-full w-full`,
                      color: "#058AAE",
                    }
              }
              textStyle={{
                fontSize: 20,
              }}
            />
          </View>
        </View>
      );
    } else {
      return (
        <View style={{ ...tw`justify-center items-center h-1/5 w-full py-3` }}>
          <View style={{ ...tw`justify-center bg-yellow-50 p-2 h-2/3 w-4/5` }}>
            <ImplementationsPicker
              implementation={implementation}
              setImplementation={(item) => {
                setImplementation(item);
                setModification(true);
              }}
              iconColor={"#058AAE"}
              pickerStyle={
                modification
                  ? {
                      ...tw`h-full w-full`,
                      color: "#009670",
                    }
                  : {
                      ...tw`h-full w-full`,
                      color: "#058AAE",
                    }
              }
              textStyle={{
                fontSize: 20,
              }}
            />
          </View>
        </View>
      );
    }
  };

  if (!user) return <View />;

  return (
    <View
      style={{
        ...tw`flex-col justify-center items-center w-full h-full bg-primary`,
      }}
    >
      <View
        style={{ ...tw`flex-col w-4/5 h-5/6 bg-accent rounded-lg shadow-2xl` }}
      >
        <View style={{ ...tw`flex-col h-full w-full` }}>
          <View style={{ ...tw`justify-end items-center w-full h-3/24` }}>
            <AppText style={{ ...tw`text-center text-primary`, fontSize: 30 }}>
              Modification Profil
            </AppText>
          </View>
          <View style={{ ...tw`justify-center items-centerflex h-3/4 w-full` }}>
            <View
              style={{ ...tw`justify-center items-center h-1/5 w-full py-3` }}
            >
              <View style={{ ...tw`bg-yellow-50 p-2 h-2/3 w-4/5 py-1` }}>
                <TextInput
                  style={{
                    ...tw`h-full w-full`,
                    fontFamily: "GentiumBasic_400Regular",
                    fontSize: 20,
                    color: "#009670",
                  }}
                  placeholderTextColor="#058AAE"
                  placeholder={user.last_name}
                  onChangeText={(lastName) => {
                    if (lastName === "") {
                      setModification(false);
                    } else {
                      setModification(true);
                      setLastName(lastName);
                    }
                  }}
                />
              </View>
            </View>
            <View
              style={{ ...tw`justify-center items-center h-1/5 w-full py-3` }}
            >
              <View style={{ ...tw`bg-yellow-50 p-2 h-2/3 w-4/5 py-1` }}>
                <TextInput
                  style={{
                    ...tw`h-full w-full`,
                    fontFamily: "GentiumBasic_400Regular",
                    fontSize: 20,
                    color: "#009670",
                  }}
                  placeholderTextColor="#058AAE"
                  placeholder={user.first_name}
                  onChangeText={(firstName) => {
                    if (firstName === "") {
                      setModification(false);
                    } else {
                      setModification(true);
                      setFirstName(firstName);
                    }
                  }}
                />
              </View>
            </View>
            <View
              style={{ ...tw`justify-center items-center h-1/5 w-full py-3` }}
            >
              <View style={{ ...tw`bg-yellow-50 p-2 h-2/3 w-4/5 py-1` }}>
                <TextInput
                  style={{
                    ...tw`h-full w-full`,
                    fontFamily: "GentiumBasic_400Regular",
                    fontSize: 20,
                    color: "#009670",
                  }}
                  secureTextEntry={true}
                  placeholderTextColor="#058AAE"
                  placeholder="Mot de passe"
                  onChangeText={(passWord) => {
                    if (passWord === "") {
                      setModification(false);
                    } else {
                      setModification(true);
                      setPassWord(passWord);
                    }
                  }}
                />
              </View>
            </View>
            <View
              style={{ ...tw`justify-center items-center h-1/5 w-full py-3` }}
            >
              <View style={{ ...tw`bg-yellow-50 p-2 h-2/3 w-4/5 py-1` }}>
                <TextInput
                  style={{
                    ...tw`h-full w-full`,
                    fontFamily: "GentiumBasic_400Regular",
                    fontSize: 20,
                    color: "#009670",
                  }}
                  secureTextEntry={true}
                  placeholderTextColor="#058AAE"
                  placeholder="Confirmer mot de passe"
                  onChangeText={(confirmation) => setConfirmation(confirmation)}
                />
              </View>
            </View>
            <Implementations />
          </View>
          <View style={{ ...tw`flex h-3/24 w-full` }}>
            <TextButton
              containerStyle={{
                ...tw`flex justify-center items-center h-full w-full`,
              }}
              buttonStyle={{
                ...tw`bg-yellow-50 justify-center border-t-2 border-primary items-center rounded-b-lg h-full w-full`,
              }}
              text={modification ? "Modifier" : "Revenir au Profil"}
              textStyle={
                modification
                  ? { fontSize: 30, color: "#009670" }
                  : { fontSize: 24, color: "#058AAE" }
              }
              touchColor="#058AAE"
              action={() => {
                if (modification) {
                  if (verification) updateProfile();
                }
                navigation.goBack();
              }}
            />
          </View>
        </View>
      </View>
    </View>
  );
};
