import React, { useState, useEffect } from "react";
import { Image, View, Platform } from "react-native";
import * as ImagePicker from "expo-image-picker";
import tw from "../utils/tailwind";
import { TextButton } from "../components/AppButton";
import { UploadImage } from "../../firebase";
import { manipulateAsync } from "expo-image-manipulator";
import { saveProfilPictureQuery } from "../queries/media";

export const PhotoProfile = ({ navigation }) => {
  const [image, setImage] = useState(null);
  const mutation = saveProfilPictureQuery({
    options: {
      onSuccess: (data) => console.log(data),
    },
  });

  const saveImage = async () => {
    const resizedImage = await manipulateAsync(image, [], {
      compress: 1,
    }).then();
    const href = await UploadImage(resizedImage.uri);
    mutation.mutate(href);
    navigation.goBack();
  };
  const pickImage = async () => {
    if (Platform.OS !== "web") {
      const { status } =
        await ImagePicker.requestMediaLibraryPermissionsAsync();
      if (status === "granted") {
        let result = await ImagePicker.launchImageLibraryAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.All,
          allowsEditing: true,
          aspect: [4, 3],
          quality: 1,
        });
        if (!result.cancelled) {
          setImage(result.uri);
        }
      } else {
        alert(
          "Désolé, nous avons besoin de l'accès à votre librairie de photo"
        );
      }
    } else {
      alert(
        "Désolé, ces fonctionnalités ne sont pas adapté au web, n'hésitez pas à télécharger notre application sur android ou IOS"
      );
    }
  };

  const takePhoto = async () => {
    if (Platform.OS !== "web") {
      const { status } = await ImagePicker.requestCameraPermissionsAsync();
      if (status === "granted") {
        let result = await ImagePicker.launchCameraAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.All,
          allowsEditing: true,
          aspect: [4, 3],
          quality: 1,
        });

        if (!result.cancelled) {
          setImage(result.uri);
        } else {
          alert("Désolé, nous avons besoin de l'accès à votre caméra");
        }
      } else {
        alert(
          "Désolé, ces fonctionnalités ne sont pas adapté au web, n'hésitez pas à télécharger notre application sur android ou IOS"
        );
      }
    }
  };

  return (
    <View style={{ ...tw`flex-col w-full h-full` }}>
      <View style={{ ...tw`flex h-5/12 w-full` }}>
        <View
          style={{
            ...tw`flex justify-center items-center`,
          }}
        >
          {image && (
            <Image source={{ uri: image }} style={{ ...tw`w-full h-full` }} />
          )}
        </View>
      </View>
      <View
        style={{
          ...tw`flex-col justify-start h-8/12 w-full bg-yellow-50 absolute bottom--6`,
          borderTopRightRadius: 20,
          borderTopLeftRadius: 20,
        }}
      >
        <View
          style={{ ...tw`flex-col justify-center items-center w-full h-full` }}
        >
          <TextButton
            action={pickImage}
            textStyle={{
              ...tw`text-primary text-center text-lg`,
              letterSpacing: 0.5,
            }}
            containerStyle={{
              ...tw`flex justify-center p-2`,
            }}
            buttonStyle={{
              ...tw`bg-accent p-2 rounded`,
            }}
            touchColor="#006f91"
            text="Choisir une image"
          />
          <TextButton
            action={takePhoto}
            textStyle={{
              ...tw`text-primary text-center text-lg`,
              letterSpacing: 0.5,
            }}
            containerStyle={{
              ...tw`flex justify-center p-2`,
            }}
            buttonStyle={{
              ...tw`bg-accent p-2 rounded`,
            }}
            touchColor="#006f91"
            text="Prendre une photo"
          />
          <TextButton
            action={saveImage}
            textStyle={{
              ...tw`text-primary text-center text-lg`,
              letterSpacing: 0.5,
            }}
            containerStyle={{
              ...tw`flex justify-center p-2`,
            }}
            buttonStyle={{
              ...tw`bg-accent p-2 rounded`,
            }}
            touchColor="#006f91"
            text="Enregistrer"
          />
        </View>
      </View>
    </View>
  );
};
