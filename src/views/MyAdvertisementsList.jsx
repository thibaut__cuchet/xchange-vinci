import React, { useState, useEffect } from "react";
import { View, Text } from "react-native";
import { AdvertisementsList } from "../components/AdvertisementsList";
import { ReturnButton } from "../components/ReturnButton";
import { myProductsQuery } from "../queries/products";
import tw from "../utils/tailwind";

export const MyAdvertisementsList = ({ navigation }) => {
  const [advertisements, setAdvertisements] = useState([]);
  const query = myProductsQuery({});

  useEffect(() => {
    if (query.isFetched) {
      setAdvertisements(query.data.data.data.advertisements);
    }
  }, [query.isFetched]);

  return (
    <View>
      <View style={tw`m-4`}>
        <ReturnButton
          onPress={() => {
            navigation.goBack();
          }}
        />
      </View>
      <View style={tw`pb-24`}>
        <AdvertisementsList
          query={query}
          advertisements={advertisements}
          ListHeaderComponent={() => <View style={tw`h-14`}></View>}
        />
      </View>
    </View>
  );
};
