import React, { useState, useEffect } from "react";

import { Image, View, TouchableOpacity } from "react-native";
import {
  InfoIcon,
  ModifyIcon,
  ModifyPhotoIcon,
  AdminIcon,
} from "../components/Icons";
import { AppText } from "../components/AppText";
import { TextButton } from "../components/AppButton";
import tw from "../utils/tailwind";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { currentUserQuery } from "../queries/users";
import jwtDecode from "jwt-decode";
import { useQueryClient } from "react-query";

export const Profile = ({ stackNavigation }) => {
  const [moderator, setModerator] = useState(false);
  const query = currentUserQuery({});
  const queryClient = useQueryClient();

  useEffect(async () => {
    const token = await AsyncStorage.getItem("TOKEN");
    const decoded = jwtDecode(token);
    setModerator(
      decoded["https://hasura.io/jwt/claims"]["x-hasura-default-role"] ===
        "moderator"
    );
  }, []);

  if (query.isLoading) return <View />;
  const user = query.data.data.data.users[0];

  return (
    <View style={{ ...tw`flex-col w-full h-full pb-10` }}>
      <View style={{ ...tw`flex h-5/12 w-full` }}>
        <View
          style={{
            ...tw`flex justify-center items-center`,
          }}
        >
          <Image
            source={{ uri: user.picture }}
            style={{ ...tw`h-full w-full` }}
          />
          <View
            style={{
              ...tw`absolute top-10 right-4`,
            }}
          >
            <TouchableOpacity
              onPress={() => stackNavigation.navigate("PhotoProfile")}
              style={{
                ...tw`rounded-full h-12 w-12 justify-center items-center bg-accent bg-opacity-40`,
              }}
            >
              <ModifyPhotoIcon color="#006f91" style={{ ...tw`h-10 w-10` }} />
            </TouchableOpacity>
          </View>
          {moderator && (
            <View
              style={{
                ...tw`absolute top-10 left-4`,
              }}
            >
              <TouchableOpacity
                onPress={() => stackNavigation.navigate("AdminPage")}
                style={{
                  ...tw`rounded-full h-12 w-12 justify-center items-center bg-accent bg-opacity-40`,
                }}
              >
                <AdminIcon color="#006f91" style={{ ...tw`h-10 w-10` }} />
              </TouchableOpacity>
            </View>
          )}
        </View>
      </View>
      <View
        style={tw`flex justify-between h-7/12 w-full bg-yellow-50 absolute bottom-10 rounded-t-2xl p-4 pb-10`}
      >
        <View style={tw`flex-1 flex items-center flex-row`}>
          <ProfileText text={user.last_name + " " + user.first_name} />
          <TouchableOpacity
            onPress={() => stackNavigation.navigate("EditProfile")}
            style={{
              ...tw`rounded-lg h-10 w-10 justify-center items-center bg-accent bg-opacity-40`,
            }}
          >
            <ModifyIcon color="#006f91" style={{ ...tw`h-10 w-10` }} />
          </TouchableOpacity>
        </View>
        <ProfileText text={user.email} />
        <ProfileText text={user.implementation.name} />
        <ProfileText text={user.implementation.campus} />
        <View style={tw`flex-1 justify-between items-center flex flex-row`}>
          <ProfileButton
            action={() => stackNavigation.navigate("MyOrders")}
            text="Mes commandes"
          />
          <ProfileButton
            action={() => stackNavigation.navigate("MyAdvertisements")}
            text="Mes annonces"
          />
        </View>
        <View style={tw`flex-1 justify-between items-center flex flex-row`}>
          <ProfileButton
            action={() => stackNavigation.navigate("Informations")}
            text="Infos"
          />
          <ProfileButton
            action={async () => {
              queryClient.invalidateQueries("currentUser");
              AsyncStorage.removeItem("TOKEN");
              stackNavigation.navigate("Login");
            }}
            text="Déconnexion"
          />
        </View>
      </View>
    </View>
  );
};

const ProfileText = ({ text }) => {
  return (
    <View style={tw`flex-1 flex justify-center`}>
      <AppText style={tw`text-xl`}>{text}</AppText>
    </View>
  );
};

const ProfileButton = ({ action, text }) => {
  return (
    <TextButton
      action={action}
      textStyle={{
        ...tw`text-primary text-center`,
        fontSize: 20,
        letterSpacing: 0.5,
      }}
      containerStyle={{
        ...tw`flex flex-1 px-2 justify-center`,
      }}
      buttonStyle={{
        ...tw`bg-accent p-2 rounded`,
      }}
      touchColor="#006f91"
      text={text}
    />
  );
};
