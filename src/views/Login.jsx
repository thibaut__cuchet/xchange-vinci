import React, { useRef, useState } from "react";
import { View, Image, TextInput, KeyboardAvoidingView } from "react-native";
import tw from "../utils/tailwind";
import { AppText } from "../components/AppText";
import { loginRequest } from "../services/authentification";
import { useMutation } from "react-query";
import { TextButton } from "../components/AppButton";
import { DismissKeyboard } from "../components/DismissKeyboard";
import AsyncStorage from "@react-native-async-storage/async-storage";

export const Login = ({ navigation }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const ref_input2 = useRef();
  const mutation = useMutation((loginInfo) => loginRequest(loginInfo), {
    onSuccess: async (response) => {
      const data = response.data;
      if (!data.data) return;
      if (data.data.login.token) {
        AsyncStorage.setItem("TOKEN", data.data.login.token);
        navigation.replace("Home");
      }
    },
  });
  const handleSubmit = () => {
    mutation.mutate({
      email,
      password,
    });
  };
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
    >
      <DismissKeyboard>
        <View
          style={tw`bg-primary h-full w-full flex flex-col items-center justify-center`}
        >
          <View style={tw`flex justify-center items-center w-full pb-5`}>
            <Image
              source={require("../../assets/vinci.png")}
              style={tw`h-42 w-47`}
            />
          </View>
          <View style={tw`flex justify-start items-center w-3/4`}>
            <AppText style={tw`text-yellow-50 text-5xl`}>Connexion</AppText>
          </View>
          <View style={tw`w-3/4 h-1/2 flex justify-center`}>
            <View
              style={tw`flex justify-center items-center w-full h-4/5 bg-accent rounded-lg shadow-2xl`}
            >
              <View style={tw`justify-center w-full h-4/5 p-6`}>
                <TextInput
                  style={{
                    ...tw`bg-yellow-50 my-4  p-3 rounded-lg shadow-lg`,
                    fontFamily: "GentiumBasic_400Regular",
                    fontSize: 18,
                    color: "#009670",
                  }}
                  placeholderTextColor="#006f91"
                  placeholder="Adresse mail"
                  onChangeText={setEmail}
                  returnKeyType="next"
                  onSubmitEditing={() => ref_input2.current.focus()}
                  keyboardType="email-address"
                  autoCapitalize="none"
                  autoComplete="email"
                />
                <TextInput
                  style={{
                    ...tw`bg-yellow-50 my-4  p-3 rounded-lg shadow-lg`,
                    fontFamily: "GentiumBasic_400Regular",
                    fontSize: 18,
                    color: "#009670",
                  }}
                  secureTextEntry={true}
                  placeholderTextColor="#006f91"
                  placeholder="Mot de passe"
                  onChangeText={setPassword}
                  ref={ref_input2}
                  onSubmitEditing={handleSubmit}
                />
                <View style={tw`items-center`}>
                  <TextButton
                    textStyle={{
                      fontSize: 14,
                      color: "#006f91",
                      textDecorationLine: "underline",
                    }}
                    text="Pas encore de compte ? Enregistez-vous ici !"
                    action={() => navigation.navigate("Register")}
                  />
                </View>
              </View>
              <View
                style={tw`bg-white w-full h-1/5 rounded-b-lg  justify-center`}
              >
                <TextButton
                  containerStyle={{
                    ...tw`flex justify-center items-center h-full w-full`,
                  }}
                  buttonStyle={{
                    ...tw`bg-yellow-50 justify-center border-t-2 border-primary items-center rounded-b-lg h-full w-full`,
                  }}
                  text="Connexion"
                  textStyle={{ fontSize: 22, color: "#058AAE" }}
                  touchColor="#058AAE"
                  action={handleSubmit}
                />
              </View>
            </View>
          </View>
        </View>
      </DismissKeyboard>
    </KeyboardAvoidingView>
  );
};
