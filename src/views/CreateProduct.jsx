import React, { useState, useRef } from "react";
import tw from "../utils/tailwind";
import { AppText } from "../components/AppText";
import {
  View,
  ScrollView,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView,
  SafeAreaView,
  Image,
} from "react-native";
import { Video } from "expo-av";
import { ImplementationsPicker } from "../components/ImplementationsPicker";
import { insertProductQuery } from "../queries/products";
import { useQueryClient } from "react-query";
import { ModifyPhotoIcon } from "../components/Icons";
import { TextButton } from "../components/AppButton";
import { ReturnButton } from "../components/ReturnButton";
import { UploadImage } from "../../firebase";
import { manipulateAsync } from "expo-image-manipulator";
import { VideoApp } from "../components/VideoApp";
import { CategoryPicker } from "../components/CategoryPicker";
import { SwipeablePanel } from "rn-swipeable-panel";

export const CreateProduct = ({ navigation, route }) => {
  const [medias, setMedias] = useState([]);
  const [selectedCampus, setSelectedCampus] = useState("");
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [iosImplementation, setIosImplementation] = useState(false);
  const videoRef = useRef(null);
  const [videoStatus, setVideoStatus] = useState({});
  const [category, setCategory] = useState(null);
  const [selectCat, setSelectCat] = useState(false);
  /* const [component, setComponent] = useState(
    <CategoryPicker setCategory={setCategory} />,
  )*/
  //const component = <CategoryPicker setCategory={setCategory} />

  const queryClient = useQueryClient();
  const mutation = insertProductQuery({
    options: {
      onSuccess: () => queryClient.invalidateQueries("allProducts"),
    },
  });

  useRef(() => console.log(medias), [medias]);

  const Implementations = () => {
    if (Platform.OS === "ios") {
      if (iosImplementation === false) {
        return (
          <View style={tw`bg-primary w-95/100 rounded-lg mx-2`}>
            <TextButton
              action={() => setIosImplementation(true)}
              textStyle={{
                ...tw`text-yellow-50 text-center`,
                fontSize: 18,
              }}
              containerStyle={{
                ...tw`flex justify-center`,
              }}
              buttonStyle={{
                ...tw`bg-primary p-2 rounded`,
              }}
              touchColor="#006f91"
              text="Sélectionner une implantation"
            />
          </View>
        );
      }
      return (
        <View
          style={tw`absolute justify-center items-center bg-yellow-50 border-4 border-primary w-full h-full bottom-0 z-10`}
        >
          <View style={tw`bg-yellow-50 w-full px-2 mx-2 `}>
            <ImplementationsPicker
              implementation={selectedCampus}
              setImplementation={(item) => {
                setSelectedCampus(item);
                setIosImplementation(false);
              }}
              iconColor={"#058AAE"}
              pickerStyle={{
                color: "#058AAE",
              }}
            />
          </View>
        </View>
      );
    } else {
      return (
        <View style={tw`bg-primary w-95/100 rounded-lg px-2 mx-2 `}>
          <ImplementationsPicker
            implementation={selectedCampus}
            setImplementation={setSelectedCampus}
            iconColor={"white"}
            pickerStyle={{
              color: "white",
            }}
          />
        </View>
      );
    }
  };

  const handleOnPress = async () => {
    if (title === "" || description === "") {
      alert("Le titre , le prix et la description ne peuvent pas être vide.");
    } else {
      const tab = medias.map(async (media) => {
        const resizedMedia = await manipulateAsync(media.uri, [], {
          compress: 1,
        }).then();
        const href = await UploadImage(resizedMedia.uri).then();
        return {
          is_video: media.type === "video" ? true : false,
          url: `${href}`,
        };
      });
      mutation.mutate({
        title,
        description,
        price,
        implementation: selectedCampus,
        medias: JSON.stringify(await Promise.all(tab).then()).replace(
          /"([^"]+)":/g,
          "$1:"
        ),
      });

      navigation.goBack();
      alert(
        "Votre annonce a bien été ajouté ,\n retrouvé la dans l'onglet 'mes annonces' sur votre profil."
      );
    }
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
    >
      <View style={tw`flex h-full self-stretch bg-yellow-50`}>
        <View
          style={{
            ...tw`absolute top-10 right-4 z-10`,
          }}
        >
          <TouchableOpacity
            onPress={() => {
              navigation.navigate("PhotoProduct", {
                giveMedia: (media) => setMedias(medias.concat(media)),
              });
            }}
            style={{
              ...tw`rounded-full h-12 w-12 justify-center items-center bg-accent bg-opacity-40`,
            }}
          >
            <ModifyPhotoIcon color="#006f91" style={{ ...tw`h-10 w-10` }} />
          </TouchableOpacity>
        </View>
        <View style={tw`absolute m-4  z-10`}>
          <ReturnButton
            onPress={() => {
              navigation.goBack();
            }}
          />
        </View>
        <SafeAreaView style={tw`h-3/10 `}>
          <ScrollView horizontal={true}>
            {medias.map((media) => (
              <Media media={media} videoRef={videoRef} />
            ))}
          </ScrollView>
        </SafeAreaView>
        <ScrollView style={tw`flex h-7/10 self-stretch`}>
          <View style={tw` flex my-2 `}>
            <View style={tw`flex  `}>
              <AppText style={tw`py-2 mx-2`}>Titre</AppText>
              <TextInput
                style={tw` w-9/10 px-2  bg-gray-200 rounded-lg mx-2`}
                placeholder={
                  "Mon plus beau titre pour la plus belle des annonces"
                }
                onChangeText={(titre) => setTitle(titre)}
              />
            </View>
            <View>
              <AppText style={tw`py-2 mx-2`}>Prix</AppText>
              <TextInput
                keyboardType="numeric"
                style={tw` w-9/10 px-2 bg-gray-200 rounded-lg mx-2`}
                placeholder={"15"}
                onChangeText={(prix) => setPrice(prix)}
              />
            </View>
            <View>
              <AppText style={tw`py-2 mx-2`}>Description </AppText>
              <View>
                <TextInput
                  multiline={true}
                  style={tw` w-9/10 px-2 bg-gray-200 rounded-lg mx-2`}
                  placeholder={"Bonjour , \n je vends ... parce que ..."}
                  onChangeText={(des) => setDescription(des)}
                />
              </View>
            </View>
          </View>
          <Implementations />
          <View>
            <TouchableOpacity
              activeOpacity={0.5}
              onPress={() => handleOnPress()}
            >
              <View style={tw`bg-primary rounded-lg p-2 my-3 mx-2`}>
                <AppText style={tw`text-white text-center text-lg`}>
                  Ajouter
                </AppText>
              </View>
            </TouchableOpacity>
          </View>
        </ScrollView>
        <View style={tw`my-2`}>
          {selectCat ? (
            <ShowCategory
              setCategory={setCategory}
              setSelectCat={setSelectCat}
            />
          ) : (
            <Category setSelectCat={setSelectCat} />
          )}
        </View>
      </View>
    </KeyboardAvoidingView>
  );
};

const Media = ({ media, videoRef }) => {
  const [videoStatus, setVideoStatus] = useState({});

  if (media.type === "image")
    return (
      <Image
        style={tw`h-9/10 w-40`}
        source={{
          uri: media.uri,
        }}
      />
    );

  return (
    <Video
      ref={videoRef}
      style={tw`h-9/10 w-40`}
      source={{ uri: media.uri }}
      useNativeControls
      resizeMode="contain"
      isLooping
      onPlaybackStatusUpdate={(videoStatus) =>
        setVideoStatus(() => videoStatus)
      }
    />
  );
};
const Category = ({ setSelectCat }) => (
  <View style={tw`bg-primary w-95/100 rounded-lg mx-2`}>
    <TextButton
      action={() => setSelectCat(true)}
      textStyle={{
        ...tw`text-yellow-50 text-center`,
        fontSize: 18,
      }}
      containerStyle={{
        ...tw`flex justify-center`,
      }}
      buttonStyle={{
        ...tw`bg-primary p-2 rounded`,
      }}
      touchColor="#006f91"
      text="Sélectionner une catégorie"
    />
  </View>
);

const ShowCategory = ({ setCategory, setSelectCat }) => (
  <SwipeablePanel
    fullWidth={true}
    isActive={true}
    closeOnTouchOutside={false}
    showCloseButton={false}
    style={tw`bg-primary z-10`}
    onClose={(category) => {
      setCategory(category), setSelectCat(false), console.log(category);
    }}
    onlySmall={true}
  >
    <CategoryPicker setCategory={setCategory} />
  </SwipeablePanel>
);
