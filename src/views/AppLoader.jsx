import React, { useState, useEffect } from "react";
import { FirstLaunch } from "./FirstLaunch";
import AsyncStorage from "@react-native-async-storage/async-storage";
import AppLoading from "expo-app-loading";
import { isJwtExpired } from "jwt-check-expiration";

export const AppLoader = ({ navigation }) => {
  const [launched, setLaunched] = useState(false);

  useEffect(async () => {
    const alreadyLaunched = await AsyncStorage.getItem("LAUNCHED");
    if (!alreadyLaunched) {
      navigation.push("Login");
      navigation.navigate("Informations");
    } else {
      isTokenExpired();
    }
  }, []);

  const isTokenExpired = async () => {
    try {
      const token = await AsyncStorage.getItem("TOKEN");
      if (token !== null) {
        if (!isJwtExpired(token)) {
          return navigation.replace("Home");
        }
      }
      return navigation.replace("Login");
    } catch (error) {
      console.log(error);
    }
  };

  return <AppLoading />;
};
