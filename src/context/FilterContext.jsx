import React, { useState, useEffect } from "react";
import { useQueryClient } from "react-query";
import { CategoriesFilter } from "../components/CategoriesFilter";

const Context = React.createContext(null);

const FilterContext = (props) => {
  const [filters, setFilters] = useState({
    category: null,
    price: null,
    campus: null,
  });

  const queryClient = useQueryClient();

  useEffect(() => {
    queryClient.invalidateQueries("allProducts");
  }, [filters]);

  const [component, setComponent] = useState(null);

  const updateFilter = (name, value) => {
    const newFilters = { ...filters };
    newFilters[name] = value;
    setFilters(newFilters);
  };

  const value = {
    filters,
    updateFilter,
    component,
    setComponent,
  };

  return <Context.Provider value={value}>{props.children}</Context.Provider>;
};

export { Context, FilterContext };

export default Context;
