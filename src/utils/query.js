import AsyncStorage from "@react-native-async-storage/async-storage";
import jwtDecode from "jwt-decode";

export const queryBuilders = ({
  type,
  name,
  table,
  fields,
  variables,
  where,
  object,
  objects,
  del,
  set,
}) => {
  let params = "";

  if (where || object || del || set) {
    params += `(${del ? del : ""}${set ? `_set: {${set}}, ` : ""}${
      where ? `where: {${where}}` : ""
    }${object ? `object: {${object}}` : ""}${objects ? `objects: ` : ""})`;
  }

  return `
    ${type} ${name}${variables ? `(${variablesToParams(variables)})` : ""} {
        ${table}${params} {
          ${fields}
        }
    }
  `;
};

const variablesToParams = (variables = []) => {
  return variables.map((variable) => `${variable.name}: ${variable.type}`);
};

export const filtersToVariables = (filters) => {
  const variables = [];
  if (filters.campus) {
    variables.push({
      name: "$campus",
      type: "Int",
    });
  }

  if (filters.category) {
    variables.push({
      name: "$category",
      type: "Int",
    });
  }

  if (filters.price) {
    if (filters.price.lte || filters.price.lte === 0) {
      variables.push({
        name: "$lte",
        type: "float8",
      });
    }

    if (filters.price.gt || filters.price.gt === 0) {
      variables.push({
        name: "$gt",
        type: "float8",
      });
    }
  }
  return variables;
};

export const filtersAndSearchToWhere = (filters, search) => {
  const where = [];
  where.push(`title: {_ilike: "%${search}%" }, state: {_eq: "published"}`);

  if (filters.campus)
    where.push(
      "advertisements_implementations: {id_implementations: {_eq: $campus}}"
    );
  if (filters.category)
    where.push("categories_advertisements: {id_category: {_eq: $category}}");
  if (filters.price) {
    let price = [];
    if (filters.price.lte || filters.price.lte === 0) price.push("_lte: $lte");
    if (filters.price.gt || filters.price.gt === 0) price.push("_gt: $gt");

    where.push(`price: {${price.join(",")}}`);
  }
  return where.join(",");
};

export const getIdFromToken = async () => {
  const token = await AsyncStorage.getItem("TOKEN");
  const decoded = jwtDecode(token);
  return Number(decoded["https://hasura.io/jwt/claims"]["x-hasura-user-id"]);
};

export const getToken = async () => {
  return await AsyncStorage.getItem("TOKEN");
};
