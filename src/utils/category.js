export const categoryToArray = (categories) => {
  const result = [];
  categories.forEach((category) => {
    if (category.parent) {
      result.find((cat) => cat.id == category.parent).data.push(category);
    } else {
      result.push({
        ...category,
        data: [],
      });
    }
  });
  return result;
};
