import { Picker } from "@react-native-picker/picker";
import React, { useState, useEffect } from "react";
import {
  FlatList,
  SectionList,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import Svg, { Path } from "react-native-svg";
import { allCategoriesQuery } from "../queries/categories";
import { categoryToArray } from "../utils/category";
import tw from "../utils/tailwind";
import { RightChevron } from "./Icons";

export const CategoryPicker = ({ setCategory }) => {
  const [categories, setCategories] = useState([]);
  const query = allCategoriesQuery({});

  useEffect(() => initPicker(), [query.isLoading]);

  const handleCategory = (category) => {
    if (category.categories && category.categories.length > 0) {
      setCategories(category.categories);
    } else {
      setCategory(category);
    }
  };

  const initPicker = () => {
    if (!query.isLoading) {
      setCategories(query.data.data.data.categories);
    }
  };

  if (query.isLoading) return <View />;

  return (
    <FlatList
      data={categories}
      style={tw`h-52`}
      showsVerticalScrollIndicator={false}
      ListHeaderComponent={
        <CategoryItem
          title="Retour"
          icon={{
            fillRule: "evenodd",
            d: "M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z",
            clipRule: "evenodd",
          }}
          onPress={() => setCategories(query.data.data.data.categories)}
        />
      }
      renderItem={({ item }) => {
        console.log(item);
        return (
          <CategoryItem
            title={item.name}
            icon={item.icon}
            onPress={() => handleCategory(item)}
            header={item.categories && item.categories.length > 0}
          />
        );
      }}
    />
  );
};

const CategoryItem = ({ title, icon, header, onPress }) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={tw`flex flex-row items-center`}>
        <CategoryIcon icon={icon} />
        <Text style={tw`text-lg pl-4 text-white flex-1`}>{title}</Text>
        {header && <RightChevron color="white" />}
      </View>
    </TouchableOpacity>
  );
};

const CategoryIcon = ({ icon }) => {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      style={tw`h-5 w-5`}
      viewBox="0 0 20 20"
      fill="white"
    >
      <Path {...icon} />
    </Svg>
  );
};
