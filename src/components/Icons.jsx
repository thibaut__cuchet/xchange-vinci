import React from "react";
import { View, Text } from "react-native";
import Svg, { Path } from "react-native-svg";
import tw from "../utils/tailwind";

const SvgFill = ({ color, children }) => {
  return (
    <Svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill={color}>
      {children}
    </Svg>
  );
};

export const CreateIcon = ({ color }) => {
  return (
    <SvgFill color={color}>
      <Path
        fillRule="evenodd"
        d="M10 18a8 8 0 100-16 8 8 0 000 16zm1-11a1 1 0 10-2 0v2H7a1 1 0 100 2h2v2a1 1 0 102 0v-2h2a1 1 0 100-2h-2V7z"
        clipRule="evenodd"
      />
    </SvgFill>
  );
};

export const MessageIcon = ({ color }) => {
  return (
    <SvgFill color={color}>
      <Path
        fill-rule="evenodd"
        d="M18 5v8a2 2 0 01-2 2h-5l-5 4v-4H4a2 2 0 01-2-2V5a2 2 0 012-2h12a2 2 0 012 2zM7 8H5v2h2V8zm2 0h2v2H9V8zm6 0h-2v2h2V8z"
        clip-rule="evenodd"
      />
    </SvgFill>
  );
};

export const ProfileIcon = ({ color }) => {
  return (
    <SvgFill color={color}>
      <Path
        fillRule="evenodd"
        d="M10 9a3 3 0 100-6 3 3 0 000 6zm-7 9a7 7 0 1114 0H3z"
        clipRule="evenodd"
      />
    </SvgFill>
  );
};

export const StarIcon = ({ color }) => {
  return (
    <SvgFill color={color}>
      <Path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
    </SvgFill>
  );
};

export const ViewGridIcon = ({ color }) => {
  return (
    <SvgFill color={color}>
      <Path d="M5 3a2 2 0 00-2 2v2a2 2 0 002 2h2a2 2 0 002-2V5a2 2 0 00-2-2H5zM5 11a2 2 0 00-2 2v2a2 2 0 002 2h2a2 2 0 002-2v-2a2 2 0 00-2-2H5zM11 5a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2V5zM11 13a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2v-2z" />
    </SvgFill>
  );
};

export const RightChevron = ({ color }) => {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      style={tw`h-8 w-8`}
      fill="none"
      viewBox="0 0 24 24"
      stroke={color}
    >
      <Path
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={2}
        d="M9 5l7 7-7 7"
      />
    </Svg>
  );
};

export const LeftChevron = ({ color }) => {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      style={tw`h-8 w-8`}
      fill="none"
      viewBox="0 0 24 24"
      stroke={color}
    >
      <Path
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={2}
        d="M15 19l-7-7 7-7"
      />
    </Svg>
  );
};

export const Check = ({ color }) => {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      style={tw`h-8 w-8`}
      fill="none"
      viewBox="0 0 24 24"
      stroke={color}
    >
      <Path
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={2}
        d="M5 13l4 4L19 7"
      />
    </Svg>
  );
};

export const FilterIcon = ({ color }) => {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      style={tw`h-8 w-8`}
      fill="#058AAE"
      viewBox="0 0 20 20"
      stroke={color}
      strokeWidth={2}
    >
      <Path
        fillRule="evenodd"
        d="M3 3a1 1 0 011-1h12a1 1 0 011 1v3a1 1 0 01-.293.707L12 11.414V15a1 1 0 01-.293.707l-2 2A1 1 0 018 17v-5.586L3.293 6.707A1 1 0 013 6V3z"
        clipRule="evenodd"
      />
    </Svg>
  );
};

export const ModifyPhotoIcon = ({ color, style }) => {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      style={style}
      fill="none"
      viewBox="0 0 24 24"
      stroke={color}
    >
      <Path
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={2}
        d="M3 9a2 2 0 012-2h.93a2 2 0 001.664-.89l.812-1.22A2 2 0 0110.07 4h3.86a2 2 0 011.664.89l.812 1.22A2 2 0 0018.07 7H19a2 2 0 012 2v9a2 2 0 01-2 2H5a2 2 0 01-2-2V9z"
      />
      <Path
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={2}
        d="M15 13a3 3 0 11-6 0 3 3 0 016 0z"
      />
    </Svg>
  );
};

export const ModifyIcon = ({ color, style }) => {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      style={style}
      fill="none"
      viewBox="0 0 24 24"
      stroke={color}
    >
      <Path
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={2}
        d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z"
      />
    </Svg>
  );
};

export const InfoIcon = ({ color, style }) => {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      style={style}
      fill="none"
      viewBox="0 0 24 24"
      stroke={color}
    >
      <Path
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={2}
        d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
      />
    </Svg>
  );
};

export const LeftArrow = ({ color }) => {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      style={tw`h-8 w-8`}
      fill="none"
      viewBox="0 0 24 24"
      stroke={color}
    >
      <Path
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={2}
        d="M10 19l-7-7m0 0l7-7m-7 7h18"
      />
    </Svg>
  );
};

export const AdminIcon = ({ color, style }) => {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      style={style}
      fill="none"
      viewBox="0 0 24 24"
      stroke={color}
    >
      <Path
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={2}
        d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2m-3 7h3m-3 4h3m-6-4h.01M9 16h.01"
      />
    </Svg>
  );
};

export const BanIcon = ({ color, style }) => {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      style={style}
      fill="none"
      viewBox="0 0 24 24"
      stroke={color}
    >
      <Path
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={2}
        d="M18.364 18.364A9 9 0 005.636 5.636m12.728 12.728A9 9 0 015.636 5.636m12.728 12.728L5.636 5.636"
      />
    </Svg>
  );
};
