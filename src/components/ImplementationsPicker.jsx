import React from "react";
import { Picker } from "@react-native-picker/picker";
import tw from "../utils/tailwind";
import { allImplementationsQuery } from "../queries/implementations";
import { G } from "react-native-svg";

export const ImplementationsPicker = ({
  implementation,
  setImplementation,
  pickerStyle,
  textStyle,
  iconColor,
}) => {
  const query = allImplementationsQuery({});

  return (
    <Picker
      selectedValue={implementation}
      onValueChange={(itemValue, itemIndex) => setImplementation(itemValue)}
      dropdownIconColor={iconColor}
      style={{ ...pickerStyle }}
    >
      {query.isFetched &&
        query.data.data.data.implementations.map((implementation) => (
          <Picker.Item
            key={implementation.id}
            label={`${implementation.name} - ${implementation.campus}`}
            value={implementation.id}
            style={{ ...textStyle }}
          ></Picker.Item>
        ))}
    </Picker>
  );
};
