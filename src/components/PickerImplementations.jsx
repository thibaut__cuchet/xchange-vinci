import React, { useState, useEffect } from "react";
import { View, Overlay } from "react-native";
import { allImplementationsQuery } from "../queries/implementations";

export const PickerImplementations = ({
  implementation,
  setImplementation,
  pickerStyle,
  textStyle,
  colorPlaceholder,
}) => {
  const Picker = Overlay.Picker;
  const query = allImplementationsQuery({});
  const [valueId, setValueId] = useState(-1);
  const [valueLabel, setValueLabel] = useState("Sélectionner un lieu");

  /*useEffect(() => {
    if (query.isFetched) {
      setTable(query.data.data.data.implementations);
    }
  }, []);*/

  let table = null;

  const ChargementItems = () => {
    if (query.isFetched) {
      //let table = new Array(query.data.data.data.implementations);
      /*query.data.data.data.implementations.map((implementation) => {
        let item = {
          name: implementation.name,
          campus: implementation.campus,
          label: `${implementation.name} - ${implementation.campus}`,
          value: implementation.id,
        };
        table.push(item);
      });*/
      //console.log(query.data.data.data.implementations);
      table = query.data.data.data.implementations;
    }
  };

  if (implementation) {
    setValueId(implementation.id);
    setValueLabel(`${implementation.name} - ${implementation.campus}`);
  }

  let tests = [
    { label: "NodeJs", value: 0 },
    { label: "C#", value: 1 },
    { label: "Python", value: 2 },
    { label: "Lua", value: 3 },
    { label: "C++", value: 4 },
    { label: "Erlang", value: 5 },
    { label: "Java", value: 6 },
    { label: "Visual Basic", value: 7 },
    { label: "Object-C", value: 8 },
    { label: "Perl", value: 9 },
    { label: "Go", value: 10 },
  ];

  if (query.isFetched) {
    ChargementItems();
    return (
      <View style={{ ...pickerStyle }}>
        <Picker
          value={valueId}
          placeholder={valueLabel}
          placeholderColor={colorPlaceholder}
          items={tests}
          containerStyle={{ height: 200 }}
          itemTextStyle={{ ...textStyle }}
          onConfirm={(index) => {
            /*setImplementation(selectedItem);
              setValueId(selectedVal);
              setValueLabel(selectedItem);
              console.log(selectedIdx);
              console.log(selectedVal);
              console.log(selectedItem);
              console.log(test);*/
            console.log(table);
          }}
        />
      </View>
    );
  }
  return <View />;
};
