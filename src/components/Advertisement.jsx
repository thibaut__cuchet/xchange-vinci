import React from "react";
import { FlatList, Text, View, Image, ScrollView } from "react-native";
import tw from "../utils/tailwind";

const Advertisement = ({ images, price, title }) => {
  const image =
    images.length > 0
      ? { uri: images[0].url }
      : require("../../assets/vinci.png");
  return (
    <View style={tw`flex rounded-xl overflow-hidden`}>
      <View style={tw`flex h-32 bg-white `}>
        <Image source={image} style={tw`flex h-full w-full`} />
      </View>
      <View style={tw` flex bg-primary`}>
        <Text style={tw`flex text-white self-center`}>{title}</Text>
        <Text style={tw`flex text-white self-center`}>{price}€</Text>
      </View>
    </View>
  );
};

export default Advertisement;
