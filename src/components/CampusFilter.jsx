import React, { useContext } from "react";
import { View } from "react-native";
import Context from "../context/FilterContext";
import tw from "../utils/tailwind";
import { ImplementationsPicker } from "./ImplementationsPicker";

export const CampusFilter = () => {
  const { filters, updateFilter, setComponent } = useContext(Context);

  const handleImplementation = (implementation) => {
    updateFilter("campus", implementation);
    setComponent(null);
  };

  return (
    <View style={tw`flex justify-center h-52`}>
      <View style={tw`w-3/4 self-center`}>
        <ImplementationsPicker
          implementation={filters.campus | 4}
          setImplementation={handleImplementation}
          textStyle={tw`text-white`}
        />
      </View>
    </View>
  );
};
