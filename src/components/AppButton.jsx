import React, { Children } from "react";
import { View, TouchableNativeFeedback } from "react-native";
import { AppText } from "./AppText";

export const TextButton = ({
  containerStyle,
  textStyle,
  buttonStyle,
  action,
  touchColor,
  text,
}) => {
  return (
    <View style={containerStyle}>
      <TouchableNativeFeedback
        onPress={() => {
          action();
        }}
        background={TouchableNativeFeedback.Ripple(touchColor)}
      >
        <View style={buttonStyle}>
          <AppText style={textStyle}>{text}</AppText>
        </View>
      </TouchableNativeFeedback>
    </View>
  );
};
