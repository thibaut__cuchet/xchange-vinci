import React from "react";

import { Product } from "../views/Product";
import { PhotoProfile } from "../views/PhotoProfile";
import { PhotoProduct } from "../views/PhotoProduct";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { BottomNavigation } from "./BottomNavigation";
import { CreateProduct } from "../views/CreateProduct";
import { EditProfile } from "../views/EditProfile";
import { FirstLaunch } from "../views/FirstLaunch";
import { Login } from "../views/Login";
import { Register } from "../views/Register";
import { AppLoader } from "../views/AppLoader";
import { MyOrdersList } from "../views/MyOrdersList";
import { MyAdvertisementsList } from "../views/MyAdvertisementsList";
import { AdminPage } from "../views/AdminPage";

const Stack = createNativeStackNavigator();

export const StackNavigation = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="Loader" component={AppLoader} />
      <Stack.Screen name="Home" component={BottomNavigation} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Register" component={Register} />
      <Stack.Screen name="AddProduct" component={CreateProduct} />
      <Stack.Screen name="EditProfile" component={EditProfile} />
      <Stack.Screen name="Informations" component={FirstLaunch} />
      <Stack.Screen name="MyOrders" component={MyOrdersList} />
      <Stack.Screen name="ProductView" component={Product} />
      <Stack.Screen name="PhotoProfile" component={PhotoProfile} />
      <Stack.Screen name="AdminPage" component={AdminPage} />
      <Stack.Screen name="PhotoProduct" component={PhotoProduct} />
      <Stack.Screen name="MyAdvertisements" component={MyAdvertisementsList} />
    </Stack.Navigator>
  );
};
