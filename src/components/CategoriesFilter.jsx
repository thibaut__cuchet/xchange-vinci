import React, { useContext } from "react";
import { View, Text, FlatList, TouchableOpacity } from "react-native";
import Context from "../context/FilterContext";
import tw from "../utils/tailwind";
import { CategoryPicker } from "./CategoryPicker";

export const CategoriesFilter = ({ setCategory }) => {
  const { updateFilter, setComponent } = useContext(Context);

  const handleCategory = (category) => {
    updateFilter("category", category.id);
    setComponent(null);
  };

  return (
    <View style={tw`flex items-center w-full h-full`}>
      <View style={tw`w-3/4`}>
        <CategoryPicker setCategory={handleCategory} />
      </View>
    </View>
  );
};
