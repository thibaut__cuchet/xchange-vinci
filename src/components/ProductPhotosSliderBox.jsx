import React from "react";
import { View, Image, Modal } from "react-native";
import tw from "../utils/tailwind";
import { ReturnButton } from "../components/ReturnButton";
import ImageViewer from "react-native-image-zoom-viewer";

export const ProductPhotosSliderBox = ({ setSelectedImage, images }) => {
  return (
    <View>
      <View
        style={tw`h-full w-full bg-black opacity-50 flex justify-center items-center`}
      >
        <Modal visible={true} transparent={true}>
          <View style={tw`m-4`}>
            <ReturnButton
              onPress={() => {
                setSelectedImage(null);
              }}
            />
          </View>
          <ImageViewer
            imageUrls={images}
            saveToLocalByLongPress={false}
            backgroundColor=""
            enablePreload={true}
            renderImage={(props) => <Image {...props} />}
          />
        </Modal>
      </View>
    </View>
  );
};
