import React from "react";
import { useQueryClient } from "react-query";
import Advertisement from "./Advertisement";
import { FlatList, TouchableOpacity, View } from "react-native";
import tw from "../utils/tailwind";

export const AdvertisementsList = ({
  query,
  advertisements,
  action,
  ListHeaderComponent,
}) => {
  return (
    <FlatList
      data={advertisements}
      style={tw`h-full mb-10`}
      refreshing={query.isLoading}
      onRefresh={() => query.refetch()}
      showsVerticalScrollIndicator={false}
      numColumns={2}
      keyExtractor={(item) => item.id.toString()}
      ListHeaderComponent={ListHeaderComponent}
      renderItem={({ item }) => (
        <View style={tw`w-2/5 m-5`}>
          <TouchableOpacity
            style={tw`w-full shadow-lg border-transparent rounded-lg overflow-hidden `}
            onPress={() => action(item.id)}
          >
            <Advertisement
              images={item.pictures}
              price={item.price}
              title={item.title}
            />
          </TouchableOpacity>
        </View>
      )}
    />
  );
};
