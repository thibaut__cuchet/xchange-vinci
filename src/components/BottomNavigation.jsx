import React, { useEffect } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import {
  CreateIcon,
  MessageIcon,
  ProfileIcon,
  StarIcon,
  ViewGridIcon,
} from "./Icons";
import { ProductsList } from "../views/ProductsList";
import { FavoritesList } from "../views/FavoritesList";
import { CreateProduct } from "../views/CreateProduct";
import { Messages } from "../views/Messages";
import { Profile } from "../views/Profile";
import tw from "../utils/tailwind";
import { FilterContext } from "../context/FilterContext";

const tabScreenOption = (activeColor, Icon, iconProps) => {
  return {
    tabBarActiveTintColor: activeColor,
    tabBarInactiveTintColor: "#426260",
    tabBarIcon: ({ color }) => (
      <TabIcon color={color} {...iconProps}>
        <Icon color={color} />
      </TabIcon>
    ),
  };
};

const TabIcon = ({ title, active, color, children }) => {
  return (
    <View>
      <View
        style={{
          ...tw`absolute h-full w-full opacity-50 rounded-full`,
          backgroundColor: active ? "rgba(0, 0, 0, 0.2)" : "transparent",
        }}
      />
      <View style={tw`flex flex-row items-center p-1`}>
        <View style={tw`w-10 h-10 pt-1`}>{children}</View>
        {active && <Text style={tw`text-xs text-white p-1`}>{title}</Text>}
      </View>
    </View>
  );
};

const TabButton = ({ routes, padding, descriptors, onPress, state }) => {
  return routes.map((route, index) => {
    const isFocused = state.index === index + padding;
    const { options } = descriptors[route.key];
    const routeName = route.name;

    return (
      <TouchableOpacity
        onPress={() => onPress(routeName)}
        key={routeName}
        activeOpacity={0.5}
      >
        {options.tabBarIcon({
          color: isFocused
            ? options.tabBarActiveTintColor
            : options.tabBarInactiveTintColor,
        })}
      </TouchableOpacity>
    );
  });
};

const TabBar = ({ state, descriptors, navigation, stackNavigation }) => {
  return (
    <View
      style={tw`flex flex-row h-16 w-full items-center bg-primary rounded-t-2xl absolute bottom-0`}
    >
      <View style={tw`flex flex-row justify-around flex-2`}>
        <TabButton
          routes={state.routes.slice(0, 2)}
          padding={0}
          descriptors={descriptors}
          onPress={navigation.navigate}
          state={state}
        />
      </View>
      <View style={tw`flex flex-row justify-center flex-1`}>
        <TabButton
          routes={state.routes.slice(2, 3)}
          padding={2}
          descriptors={descriptors}
          onPress={stackNavigation.navigate}
          state={state}
        />
      </View>
      <View style={tw`flex flex-row justify-around flex-2`}>
        <TabButton
          routes={state.routes.slice(3)}
          padding={3}
          descriptors={descriptors}
          onPress={navigation.navigate}
          state={state}
        />
      </View>
    </View>
  );
};

const Tab = createBottomTabNavigator();

export const BottomNavigation = ({ navigation }) => {
  const stackNavigation = navigation;

  return (
    <Tab.Navigator
      tabBar={({ state, descriptors, navigation }) => (
        <TabBar
          state={state}
          descriptors={descriptors}
          navigation={navigation}
          stackNavigation={stackNavigation}
        />
      )}
      screenOptions={{ headerShown: false }}
    >
      <Tab.Screen
        name="Products"
        children={() => (
          <FilterContext>
            <ProductsList stackNavigation={stackNavigation} />
          </FilterContext>
        )}
        options={({ navigation }) =>
          tabScreenOption("#119DB8", ViewGridIcon, {
            active: navigation.isFocused(),
            title: "Annonces",
          })
        }
      />
      <Tab.Screen
        name="Favorites"
        children={() => <FavoritesList stackNavigation={stackNavigation} />}
        options={({ navigation }) =>
          tabScreenOption("#F8AA00", StarIcon, {
            active: navigation.isFocused(),
            title: "Favoris",
          })
        }
      />
      <Tab.Screen
        name="AddProduct"
        component={CreateProduct}
        options={({ navigation }) =>
          tabScreenOption("#F8AA00", CreateIcon, navigation.isFocused())
        }
      />
      <Tab.Screen
        name="Messages"
        component={Messages}
        options={({ navigation }) =>
          tabScreenOption("#019670", MessageIcon, {
            active: navigation.isFocused(),
            title: "Messages",
          })
        }
      />
      <Tab.Screen
        name="Profile"
        children={() => <Profile stackNavigation={stackNavigation} />}
        options={({ navigation }) =>
          tabScreenOption("#E7276C", ProfileIcon, {
            active: navigation.isFocused(),
            title: "Profil",
          })
        }
      />
    </Tab.Navigator>
  );
};
