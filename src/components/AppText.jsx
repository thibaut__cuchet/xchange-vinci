import React from "react";
import { Text } from "react-native";

export const AppText = (props) => {
  return (
    <Text style={{ fontFamily: "GentiumBasic_400Regular", ...props.style }}>
      {props.children}
    </Text>
  );
};
