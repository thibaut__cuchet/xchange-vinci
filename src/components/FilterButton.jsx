import React from "react";
import { TouchableOpacity, View } from "react-native";
import tw from "../utils/tailwind";
import { FilterIcon } from "./Icons";

export const FilterButton = ({ active, onPress }) => {
  return (
    <TouchableOpacity activeOpacity={0.5} onPress={onPress}>
      <View style={tw`bg-white rounded-lg p-2 ${active && "opacity-50"}`}>
        <FilterIcon color={"#058AAE"} />
      </View>
    </TouchableOpacity>
  );
};
