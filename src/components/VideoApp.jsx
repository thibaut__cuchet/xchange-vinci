import React, { useState, useRef } from 'react'
import tw from '../utils/tailwind'
import { Video, AVPlaybackStatus } from 'expo-av'

export const VideoApp = ({ uri }) => {
  const video = useRef(null)
  const [videoStatus, setVideoStatus] = useState({})

  return (
    <Video
      ref={video}
      style={tw`h-9/10 w-40`}
      source={{ uri: { uri } }}
      useNativeControls
      resizeMode="contain"
      isLooping
      onPlaybackStatusUpdate={(videoStatus) =>
        setVideoStatus(() => videoStatus)
      }
    />
  )
}
