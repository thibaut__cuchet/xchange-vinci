import React from "react";
import { View } from "react-native";
import { FilterList } from "../components/Filter";
import { FilterButton } from "../components/FilterButton";
import { SearchBarApp } from "./SearchBarApp";
import tw from "../utils/tailwind";

export const ProductsListHeader = ({
  showFilters,
  setShowFilters,
  handleFilterOnPress,
  setSearch,
}) => {
  return (
    <View style={tw`shadow-2xl bg-primary pb-4`}>
      <View style={tw`flex flex-row items-center justify-center px-8 pt-10`}>
        <SearchBarApp setSearch={setSearch} />
        <FilterButton
          active={showFilters}
          onPress={() => setShowFilters(!showFilters)}
        />
      </View>
      {showFilters && <FilterList onPress={handleFilterOnPress} />}
    </View>
  );
};
