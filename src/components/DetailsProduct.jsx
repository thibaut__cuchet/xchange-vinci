import React from "react";
import { Text, View, TouchableOpacity } from "react-native";
import { StarIcon } from "./Icons";
import MapView, { Marker, PROVIDER_GOOGLE } from "react-native-maps";
import tw from "../utils/tailwind";
import {
  productSignalementQuery,
  toggleFavoriteQuery,
} from "../queries/products";
import { useQueryClient } from "react-query";
import * as Linking from "expo-linking";

const markerIcon = require("../../assets/vinci.png");
const HeaderDetailsProduct = ({ details }) => {
  const queryClient = useQueryClient();

  const favorite = details["users_favorites_advertisements"].length > 0;
  const mutation = toggleFavoriteQuery({
    options: {
      onSuccess: () => {
        queryClient.invalidateQueries(["productDetails", details.id]);
        queryClient.invalidateQueries("favoriteQuery");
      },
    },
  });

  const handleToggleFavorite = () => {
    mutation.mutate({
      advId: details.id,
      favorite,
    });
  };

  return (
    <View style={tw`flex flex-row w-full p-4`}>
      <View style={tw`flex-1`}>
        <Text style={tw`mx-2 font-extrabold text-xl uppercase text-black`}>
          {details.title}
        </Text>
        <Text style={tw`mx-2 uppercase text-base text-black`}>
          {details.price}€
        </Text>
      </View>
      <View>
        <TouchableOpacity
          onPress={handleToggleFavorite}
          disabled={mutation.isLoading}
          style={tw`h-14 w-14 pt-2 pl-2 bg-primary rounded-2xl shadow-2xl`}
        >
          <StarIcon color={favorite ? "yellow" : "white"} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export const DetailsProduct = ({ details }) => {
  const mutation = productSignalementQuery({});

  const latitude =
    details["advertisements_implementations"][0].implementation.latitude;
  const longitude =
    details["advertisements_implementations"][0].implementation.longitude;

  const handleSignalement = () => {
    mutation.mutate(details.id);
    alert(
      "L'annonce a bien été signalée. \nElle sera analysée par nos modérateurs sous peu."
    );
  };

  const handleContact = () => {
    Linking.openURL(
      `mailto: ${details.user.email}?subject=Contact annonce "${details.title}"`
    );
  };

  return (
    <View style={tw`flex-1 bg-yellow-50`}>
      <HeaderDetailsProduct details={details} />
      <View style={tw`flex-1`}>
        <Text style={tw`mx-1 my-1 text-gray-400`}>{details.description}</Text>
      </View>
      <View style={tw`flex py-1`}>
        <MapView
          provider={PROVIDER_GOOGLE}
          style={tw`flex h-40 mx-1 rounded-lg overflow-hidden`}
          initialRegion={{
            latitude,
            longitude,
            latitudeDelta: 0.01,
            longitudeDelta: 0.01,
          }}
          showsUserLocation={false}
          followUserLocation={false}
          zoomEnabled={true}
          rotateEnabled={false}
          scrollEnabled={false}
        >
          <Marker
            coordinate={{
              latitude,
              longitude,
            }}
          />
        </MapView>
      </View>
      <View style={tw`flex p-1 rounded-lg overflow-hidden`}>
        <Text style={tw`font-extrabold`}>Informations sur le vendeur</Text>
        <View style={tw`pl-4`}>
          <Text style={tw`py-1`}>{details.user.first_name}</Text>
          <TouchableOpacity onPress={handleContact}>
            <Text>{details.user.email}</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={tw`flex py-1 `}>
        <TouchableOpacity
          style={tw` h-10 w-9/10 bg-blue-800 self-center rounded-lg overflow-hidden`}
          onPress={handleContact}
        >
          <Text style={tw`self-center py-1 text-xl font-extrabold`}>
            Contacter
          </Text>
        </TouchableOpacity>
      </View>
      <View style={tw`flex py-1 mb-4`}>
        <TouchableOpacity
          style={tw` h-10 w-9/10 bg-red-700 self-center rounded-lg overflow-hidden`}
          onPress={handleSignalement}
        >
          <Text style={tw`self-center py-1 text-xl font-extrabold`}>
            Signaler
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
