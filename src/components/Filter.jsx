import React, { useState, useContext, useEffect } from "react";
import { FlatList, Text, TouchableOpacity, View } from "react-native";
import tw from "../utils/tailwind";
import { CategoriesFilter } from "../components/CategoriesFilter";
import { SwipeablePanel } from "rn-swipeable-panel";
import Context from "../context/FilterContext";
import { PriceFilter } from "./PriceFilter";
import { CampusFilter } from "./CampusFilter";

export const Filter = () => {
  const onClose = () => {
    setComponent(null);
  };

  const { component, setComponent } = useContext(Context);

  return (
    <SwipeablePanel
      fullWidth={true}
      isActive={component}
      closeOnTouchOutside={true}
      showCloseButton={false}
      style={tw`bg-primary z-10`}
      onClose={onClose}
    >
      {component}
    </SwipeablePanel>
  );
};

const f = [
  {
    id: "category",
    name: "Catégories",
    component: () => <CategoriesFilter />,
  },
  {
    id: "price",
    name: "Prix",
    component: () => <PriceFilter />,
  },
  {
    id: "campus",
    name: "Campus",
    component: () => <CampusFilter />,
  },
];

export const FilterList = () => {
  return (
    <View style={tw`flex items-center`}>
      <FlatList
        data={f}
        renderItem={({ item }) => <FilterItem item={item} />}
        keyExtractor={(item) => item.id}
        horizontal={true}
      />
    </View>
  );
};

const FilterItem = ({ item }) => {
  const { filters, updateFilter, setComponent } = useContext(Context);
  const [active, setActive] = useState(filters[item.id]);

  useEffect(() => {
    setActive(!filters[item.id]);
  }, [filters]);

  const handleOnPress = () => {
    if (filters[item.id]) {
      updateFilter(item.id, null);
    } else {
      setComponent(item.component);
    }
  };

  return (
    <TouchableOpacity
      activeOpacity={0.5}
      onPress={handleOnPress}
      style={tw`bg-white ${active && "opacity-50"} rounded-full mx-2`}
    >
      <Text style={tw`text-primary text-center  py-2 px-4 flex-auto mx-2`}>
        {item.name}
      </Text>
    </TouchableOpacity>
  );
};
