import React from "react";
import { View, TextInput } from "react-native";

export const AppInput = ({ props, text }) => {
  return (
    <View>
      <TextInput {...props}>{text}</TextInput>
    </View>
  );
};
