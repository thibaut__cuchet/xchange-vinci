import React from "react";
import { TouchableOpacity, View } from "react-native";
import tw from "../utils/tailwind";
import { LeftArrow } from "./Icons";

export const ReturnButton = ({ onPress }) => {
  return (
    <TouchableOpacity
      activeOpacity={0.5}
      onPress={onPress}
      style={tw`absolute top-6 left-2 z-10`}
    >
      <View style={tw`bg-primary rounded-full p-3`}>
        <LeftArrow color={"white"} />
      </View>
    </TouchableOpacity>
  );
};
