import React, { useContext } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import Context from "../context/FilterContext";
import tw from "../utils/tailwind";

export const PriceFilter = () => {
  return (
    <View style={tw`h-52 w-full flex justify-center items-center`}>
      <View style={tw`flex flex-row w-2/3 justify-around`}>
        <PriceItem title="Gratuit" lte={0} />
        <PriceItem title="€" lte={15} gt={0} />
        <PriceItem title="€€" lte={50} gt={15} />
        <PriceItem title="€€€" gt={50} />
      </View>
    </View>
  );
};

const PriceItem = ({ title, lte, gt }) => {
  const { updateFilter, setComponent } = useContext(Context);
  const handlePrice = () => {
    setComponent(null);
    updateFilter("price", {
      lte,
      gt,
    });
  };

  return (
    <TouchableOpacity
      onPress={handlePrice}
      style={tw`p-2 border-white border rounded`}
    >
      <Text style={tw`text-white text-lg`}>{title}</Text>
    </TouchableOpacity>
  );
};
