import axios from "axios";
import AsyncStorage from "@react-native-async-storage/async-storage";
import jwtDecode from "jwt-decode";

export const currentUser = async () => {
  const token = await AsyncStorage.getItem("TOKEN");
  var decoded = jwtDecode(token);
  const id = Number(
    decoded["https://hasura.io/jwt/claims"]["x-hasura-user-id"]
  );

  return axios.post(
    "https://hasura-pfe.herokuapp.com/v1/graphql",
    JSON.stringify({
      query: `
                query MyQuery($id: Int!) {
                    users(where: {id: {_eq: $id}}) {
                        email
                        first_name
                        last_name
                        picture
                        implementation {
                            id
                            name
                            campus
                        }
                    }
                }
              `,
      variables: {
        id,
      },
    }),
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
};

export const allUsers = async () => {
  const token = await AsyncStorage.getItem("TOKEN");

  return axios.post(
    "https://hasura-pfe.herokuapp.com/v1/graphql",
    JSON.stringify({
      query: `
      query MyQuery {
        users {
          email
          id
          first_name
          last_name
          is_locked
        }
      }
              `,
    }),
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
};
