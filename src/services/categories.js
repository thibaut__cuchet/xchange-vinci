import axios from "axios";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { queryBuilders } from "../utils/query";

export const allCategories = () => {
  const query = queryBuilders({
    type: "query",
    name: "AllCategory",
    table: "categories",
    where: "parent: {_is_null: true}",
    fields: `name
        parent
        icon
        id
        categories {
          icon 
          name
          parent
        }
    `,
  });

  return axios.post(
    "https://hasura-pfe.herokuapp.com/v1/graphql",
    JSON.stringify({
      query,
    })
  );
};

export const insertCategory = async ({ name, parent }) => {
  const token = await AsyncStorage.getItem("TOKEN");
  return axios.post(
    "https://hasura-pfe.herokuapp.com/v1/graphql",
    JSON.stringify({
      query: `
      mutation MyMutation($name: String!, $parent: Int!) {
        insert_categories(objects: {name: $name, parent: $parent})
      }
    `,
      variables: {
        name,
        parent,
      },
    }),
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
};
