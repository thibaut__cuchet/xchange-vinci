import axios from "axios";
import { queryBuilders, getToken, getIdFromToken } from "../utils/query";

export const saveProfilPicture = async (href) => {
  const id = await getIdFromToken();

  const query = queryBuilders({
    type: "mutation",
    name: "saveProfilPicture",
    table: "update_users",
    fields: `affected_rows`,
    variables: [
      { name: "$_eq", type: "Int!" },
      { name: "$picture", type: "String!" },
    ],
    set: "picture: $picture",
    where: "id: {_eq: $_eq}",
  });
  return axios.post(
    "https://hasura-pfe.herokuapp.com/v1/graphql",
    JSON.stringify({
      query,
      variables: {
        _eq: id,
        picture: href,
      },
    }),
    {
      headers: {
        Authorization: `Bearer ${await getToken()}`,
      },
    }
  );
};

export const getProfilImage = async () => {
  const id = await getIdFromToken();

  const query = queryBuilders({
    type: "query",
    name: "getProfilImage",
    table: "users",
    fields: `picture`,
    variables: [{ name: "$_eq", type: "Int!" }],
    where: "id: {_eq: $_eq}",
  });
  return axios.post(
    "https://hasura-pfe.herokuapp.com/v1/graphql",
    JSON.stringify({
      query,
      variables: {
        _eq: id,
      },
    }),
    {
      headers: {
        Authorization: `Bearer ${await getToken()}`,
      },
    }
  );
};
