import axios from "axios";

export const allImplementations = async () => {
  return axios.post(
    "https://hasura-pfe.herokuapp.com/v1/graphql",
    JSON.stringify({
      query: `
            query AllImplementations {
                implementations {
                    campus
                    id
                    latitude
                    longitude
                    name
                    }
            }
            `,
    })
  );
};
