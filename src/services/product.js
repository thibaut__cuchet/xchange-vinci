import axios from "axios";
import AsyncStorage from "@react-native-async-storage/async-storage";
import jwtDecode from "jwt-decode";
import {
  filtersToVariables,
  filtersAndSearchToWhere,
  queryBuilders,
  getToken,
  getIdFromToken,
} from "../utils/query";

export const allProducts = async (filters, search) => {
  const token = await AsyncStorage.getItem("TOKEN");

  const queryVariables = filtersToVariables(filters);
  const where = filtersAndSearchToWhere(filters, search);

  const query = queryBuilders({
    type: "query",
    name: "AllProducts",
    table: "advertisements",
    fields: `id
        price
        title
        pictures{
          url
        }`,
    variables: queryVariables.length > 0 ? queryVariables : null,
    where,
  });

  let variables = {};
  Object.keys(filters).forEach(
    (key) => filters[key] && (variables[key] = filters[key])
  );

  if (variables.price) {
    if (variables.price.lte || variables.price.lte === 0) {
      variables.lte = variables.price.lte;
    }

    if (variables.price.gt || variables.price.gt === 0) {
      variables.gt = variables.price.gt;
    }

    delete variables.price;
  }

  return axios.post(
    "https://hasura-pfe.herokuapp.com/v1/graphql",
    JSON.stringify({
      query,
      variables,
    }),
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
};

export const favoriteProducts = async () => {
  const token = await AsyncStorage.getItem("TOKEN");
  const decoded = jwtDecode(token);
  const id = Number(
    decoded["https://hasura.io/jwt/claims"]["x-hasura-user-id"]
  );

  return axios.post(
    "https://hasura-pfe.herokuapp.com/v1/graphql",
    JSON.stringify({
      query: `
        query FavoriteProducts($userId: Int!) {
          users_favorites_advertisements(where: {user_id: {_eq: $userId}}) {
            advertisement {
              id
              price
              title
              pictures{
                url
              }
            }
          }
        }
      `,
      variables: {
        userId: id,
      },
    }),
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
};

export const myOrders = async () => {
  const token = await AsyncStorage.getItem("TOKEN");
  const decoded = jwtDecode(token);
  const id = Number(
    decoded["https://hasura.io/jwt/claims"]["x-hasura-user-id"]
  );
  return axios.post(
    "https://hasura-pfe.herokuapp.com/v1/graphql",
    JSON.stringify({
      query: `
      query MyOrders($user_id: Int!) {
        sales(where: {user_id: {_eq: $user_id}}) {
          date
          advertisement {
            id
            description
            title
          }
        }
      }
      `,
      variables: {
        user_id: id,
      },
    }),
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
};

export const insertProduct = async ({
  title,
  description,
  price,
  implementation,
  medias,
}) => {
  const user = await getIdFromToken();
  const query = queryBuilders({
    type: "mutation",
    name: "insertProduct",
    table: "insert_advertisements_one",
    object: `title: $title, description: $description, price :$price, user_id: $user, advertisements_implementations: {data: {id_implementations: $implementation}}, pictures: {data: ${medias}}`,
    variables: [
      { name: "$title", type: "String!" },
      { name: "$description", type: "String!" },
      { name: "$price", type: "float8!" },
      { name: "$user", type: "Int!" },
      { name: "$implementation", type: "Int!" },
    ],
    fields: `id`,
  });
  return axios.post(
    "https://hasura-pfe.herokuapp.com/v1/graphql",
    JSON.stringify({
      query,
      variables: {
        title,
        description,
        price,
        implementation,
        user,
      },
    }),
    {
      headers: {
        Authorization: `Bearer ${await getToken()}`,
      },
    }
  );
};

export const productDetails = async (productId) => {
  const token = await AsyncStorage.getItem("TOKEN");
  return axios.post(
    "https://hasura-pfe.herokuapp.com/v1/graphql",
    JSON.stringify({
      query: `
        query MyQuery($productId: Int!) {
          advertisements(where: {id: {_eq: $productId}}) {
            price
            id
            title
            description
            advertisements_implementations {
              implementation {
                latitude
                longitude
              }
            }
            user {
              first_name
              email
            }
            users_favorites_advertisements {
              user_id
            }
            pictures{
              url
            }
          }
        }
      `,
      variables: {
        productId,
      },
    }),
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
};

export const productSignalement = async (advId) => {
  const token = await AsyncStorage.getItem("TOKEN");
  var decoded = jwtDecode(token);
  const id = Number(
    decoded["https://hasura.io/jwt/claims"]["x-hasura-user-id"]
  );

  return axios.post(
    "https://hasura-pfe.herokuapp.com/v1/graphql",
    JSON.stringify({
      query: `
        mutation MyMutation2($advId: Int, $userId: Int) {
          insert_signaled_advertisements(objects: {adv_id: $advId, user_id: $userId}) {
            affected_rows
          }
        }      
      `,
      variables: {
        userId: id,
        advId,
      },
    }),
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
};

export const toggleFavorite = async ({ advId, favorite }) => {
  const token = await AsyncStorage.getItem("TOKEN");
  var decoded = jwtDecode(token);
  const id = Number(
    decoded["https://hasura.io/jwt/claims"]["x-hasura-user-id"]
  );

  const query = queryBuilders({
    type: "mutation",
    name: "ToggleFavorite",
    table: favorite
      ? "delete_users_favorites_advertisements_by_pk"
      : "insert_users_favorites_advertisements_one",
    variables: [
      {
        name: "$advId",
        type: "Int!",
      },
      {
        name: "$userId",
        type: "Int!",
      },
    ],
    fields: `user_id
        adv_id`,
    [favorite ? "del" : "object"]: "adv_id: $advId, user_id: $userId",
  });

  return axios.post(
    "https://hasura-pfe.herokuapp.com/v1/graphql",
    JSON.stringify({
      query,
      variables: {
        advId,
        userId: id,
      },
    }),
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
};

export const myProducts = async () => {
  const token = await AsyncStorage.getItem("TOKEN");
  const decoded = jwtDecode(token);
  const id = Number(
    decoded["https://hasura.io/jwt/claims"]["x-hasura-user-id"]
  );

  return axios.post(
    "https://hasura-pfe.herokuapp.com/v1/graphql",
    JSON.stringify({
      query: `query MyProducts($user_id: Int!) {
        advertisements(where: {user_id: {_eq: $user_id}}) {
          description
          price
          title
          state
          id
          pictures{
            url
          }
        }
      }
      `,
      variables: {
        user_id: id,
      },
    }),
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
};

export const signaledProducts = async () => {
  const token = await AsyncStorage.getItem("TOKEN");
  return axios.post(
    "https://hasura-pfe.herokuapp.com/v1/graphql",
    JSON.stringify({
      query: `
      query MyQuery {
        signaled_advertisements(distinct_on: adv_id) {
          advertisement {
            title
            user {
              email
            }
            id
            state
          }
        }
      }
      `,
    }),
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
};

export const rejectProduct = async (productId) => {
  const query = queryBuilders({
    type: "mutation",
    name: "RejectProduct",
    table: "update_advertisements",
    variables: [{ name: "$productId", type: "Int!" }],
    where: `id: {_eq: $productId}`,
    set: `state: "rejected"`,
    fields: `affected_rows`,
  });

  return axios.post(
    "https://hasura-pfe.herokuapp.com/v1/graphql",
    JSON.stringify({
      query,
      variables: {
        productId,
      },
    }),
    {
      headers: {
        Authorization: `Bearer ${await getToken()}`,
      },
    }
  );
};

export const lockUser = async (userId) => {
  const query = queryBuilders({
    type: "mutation",
    name: "LockUser",
    table: "update_users",
    variables: [{ name: "$userId", type: "Int!" }],
    where: "id: {_eq: $userId}",
    set: "is_locked: true",
    fields: `affected_rows`,
  });

  return axios.post(
    "https://hasura-pfe.herokuapp.com/v1/graphql",
    JSON.stringify({
      query,
      variables: {
        userId,
      },
    }),
    {
      headers: {
        Authorization: `Bearer ${await getToken()}`,
      },
    }
  );
};

export const removeCategory = async (categoryId) => {
  const query = queryBuilders({
    type: "mutation",
    name: "RemoveCategory",
    table: "delete_categories",
    variables: [{ name: "$categoryId", type: "Int!" }],
    where: "id: {_eq: $categoryId}",
    fields: `affected_rows`,
  });

  return axios.post(
    "https://hasura-pfe.herokuapp.com/v1/graphql",
    JSON.stringify({
      query,
      variables: {
        categoryId,
      },
    }),
    {
      headers: {
        Authorization: `Bearer ${await getToken()}`,
      },
    }
  );
};

export const addCategory = async ({ name, parent }) => {
  const query = queryBuilders({
    type: "mutation",
    name: "AddCategory",
    table: "insert_categories_one",
    variables: [
      { name: "$name", type: "String!" },
      { name: "$parent", type: "Int!" },
    ],
    object: "name: $name, parent: $parent",
    fields: `id`,
  });

  return axios.post(
    "https://hasura-pfe.herokuapp.com/v1/graphql",
    JSON.stringify({
      query,
      variables: {
        name,
        parent,
      },
    }),
    {
      headers: {
        Authorization: `Bearer ${await getToken()}`,
      },
    }
  );
};
