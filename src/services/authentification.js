import axios from "axios";
import { useMutation } from "react-query";
import AsyncStorage from "@react-native-async-storage/async-storage";

export const loginRequest = async ({ email, password }) => {
  return axios.post(
    "https://hasura-pfe.herokuapp.com/v1/graphql",
    JSON.stringify({
      query: `
        mutation Login($email: String!, $password: String!) {
          login(email: $email, password: $password) {
            token
          }
        }
      `,
      variables: {
        email,
        password,
      },
    })
  );
};

export const registerRequest = async ({
  last_name,
  first_name,
  email,
  password,
  implementation_id,
}) => {
  return axios.post(
    "https://hasura-pfe.herokuapp.com/v1/graphql",
    JSON.stringify({
      query: `
      mutation Register($email: String!, $first_name: String!, $last_name: String!, $password: String!, $implementation_id: Int!) {
        register(user: {email: $email, first_name: $first_name, last_name: $last_name, password: $password, implementation_id: $implementation_id}) {
          token
        }
      }
    `,
      variables: {
        first_name,
        last_name,
        email,
        password,
        implementation_id,
      },
    })
  );
};
