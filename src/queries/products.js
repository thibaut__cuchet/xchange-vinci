import { useMutation, useQuery } from "react-query";
import {
  allProducts,
  favoriteProducts,
  insertProduct,
  myOrders,
  productDetails,
  productSignalement,
  toggleFavorite,
  myProducts,
  signaledProducts,
  rejectProduct,
  lockUser,
  removeCategory,
  addCategory,
} from "../services/product";

export const allProductsQuery = ({ filters, search, options }) =>
  useQuery(
    ["allProducts", filters, search],
    () => allProducts(filters, search),
    options
  );

export const favoriteProductsQuery = ({ options }) =>
  useQuery("favoriteQuery", favoriteProducts, options);

export const myOrdersQuery = ({ options }) =>
  useQuery("myOrders", myOrders, options);

export const insertProductQuery = ({ options }) =>
  useMutation(insertProduct, options);

export const productDetailsQuery = ({ productId, options }) =>
  useQuery(
    ["productDetails", productId],
    () => productDetails(productId),
    options
  );

export const productSignalementQuery = ({ options }) =>
  useMutation(productSignalement, options);

export const toggleFavoriteQuery = ({ options }) =>
  useMutation(toggleFavorite, options);

export const myProductsQuery = ({ options }) =>
  useQuery("myProducts", myProducts, options);

export const signaledProductsQuery = ({ options }) =>
  useQuery("signaledProducts", signaledProducts, options);

export const rejectProductQuery = ({ options }) =>
  useMutation(rejectProduct, options);

export const lockUserQuery = ({ options }) => useMutation(lockUser, options);

export const removeCategoryQuery = ({ options }) =>
  useMutation(removeCategory, options);

export const addCategoryQuery = ({ options }) =>
  useMutation(addCategory, options);
