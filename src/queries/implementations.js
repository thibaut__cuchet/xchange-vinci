import { useQuery } from "react-query";
import { allImplementations } from "../services/implementations";

export const allImplementationsQuery = ({ options }) =>
  useQuery("allImplementations", allImplementations, options);
