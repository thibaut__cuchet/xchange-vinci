import { useQuery } from "react-query";
import { currentUser } from "../services/users";
import { allUsers } from "../services/users";

export const currentUserQuery = ({ options }) =>
  useQuery("currentUser", currentUser, options);

export const allUsersQuery = ({ options }) =>
  useQuery("allUsers", allUsers, options);
