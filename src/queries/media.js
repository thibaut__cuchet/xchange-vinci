import { useMutation } from "react-query";
import { saveProfilPicture, getProfilImage } from "../services/media";

export const saveProfilPictureQuery = ({ options }) =>
  useMutation(saveProfilPicture, options);

export const getProfilImageQuery = ({ options }) =>
  useMutation(getProfilImage, options);
