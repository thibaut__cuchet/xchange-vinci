import { useQuery } from "react-query";
import { allCategories } from "../services/categories";
import { insertCategory } from "../services/categories";

export const allCategoriesQuery = ({ options }) =>
  useQuery("allCategories", allCategories, options);

export const addCategory = ({ options }) =>
  useQuery("insertCategory", insertCategory, options);
